package us.cobalt.lua;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import org.luaj.vm2.LuaTable;
import org.luaj.vm2.lib.PackageLib;
import org.luaj.vm2.script.LuaScriptEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by eve on 10/15/2014
 */
public final class ScriptManager {

	private final Logger logger = LoggerFactory.getLogger(ScriptManager.class);
	private final ScriptEngine engine = new LuaScriptEngine();

	public ScriptManager() {
	}

	public void appendDefaultPath(Path path) {
		PackageLib.DEFAULT_LUA_PATH += String.format(";%s?.lua", path.toString());
	}

	public void loadScript(Path path) throws IOException, ScriptException {
		engine.eval(Files.newBufferedReader(path));
	}

	public void loadDirection(Path path) throws ScriptException, IOException {
		try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path, p -> Files.isDirectory(p) || p.toString().endsWith(".lua"))) {
			for (Path p : directoryStream) {
				// If the path is a subdirectory load those files first
				if (Files.isDirectory(path)) {
					loadDirection(p);
				} else {
					if (Files.exists(path.getParent().resolve(".ignore"))) {
						continue;
					}
					engine.eval(Files.newBufferedReader(path));
				}
			}
		} catch (IOException reason) {
			logger.error("Failed to load scripts in path", reason);
			throw new IOException(reason);
		}
	}

	public void bindModule(String name, LuaTable module) {
		Bindings bindings = engine.getBindings(ScriptContext.ENGINE_SCOPE);
		bindings.put(name, module);
	}

}
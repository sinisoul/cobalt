package us.cobalt.lua;

import org.luaj.vm2.Varargs;

/**
 * Created by eve on 10/15/2014
 */
public interface VarargsClosure {

	/**
	 * Invokes the variable argument closure.
	 *
	 * @param args The arguments to pass to the closure.
	 * @return The result of the closure.
	 */
	Varargs invoke(Varargs args);

}
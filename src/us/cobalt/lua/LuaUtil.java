package us.cobalt.lua;

import java.util.Set;

import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

import com.google.common.collect.Sets;

/**
 * Created by eve on 10/15/2014
 */
public final class LuaUtil {

	private static final Set<Class<?>> allowedTypes = Sets.newHashSet(Double.class, String.class, Integer.class);

	private LuaUtil() {
	}

	/**
	 * Checks the variable arguments to see if it has between the minimum and maximum amount of arguments.
	 *
	 * @param args The variable arguments.
	 * @param min The minimum amount of arguments.
	 * @param max The maximum amount of arguments.
	 * @return If the variable arguments has between and including to the minimum and maximum arguments.
	 */
	public static boolean checkVarargs(Varargs args, int min, int max) {
		return args.narg() >= min && args.narg() <= max;
	}

	public static boolean checkVarargs(Varargs args, int min) {
		return args.narg() >= min;
	}

	public static void append(LuaTable table, String name, VarargsClosure closure) {
		table.set(name, new VarArgFunction() {
			@Override
			public Varargs invoke(Varargs varargs) {
				return closure.invoke(varargs);
			}
		});
	}

	public static <T> LuaValue getValueOf(T var) {
		if (!allowedTypes.contains(var.getClass())) {
			throw new IllegalArgumentException("Cannot convert " + var.getClass() + " to LuaValue");
		}

		if (Double.class == var.getClass()) {
			double d = (Double) var;
			return LuaValue.valueOf(d);
		}

		if (String.class == var.getClass()) {
			String s = (String) var;
			return LuaValue.valueOf(s);
		}

		if (Integer.class == var.getClass()) {
			Integer i = (Integer) var;
			return LuaValue.valueOf(i);
		}

		throw new RuntimeException("Unhandled type in <T> getValueOf(T)" + var.getClass());
	}

}
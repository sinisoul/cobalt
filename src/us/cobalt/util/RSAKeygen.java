package us.cobalt.util;

import com.google.gson.*;

import java.io.FileWriter;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

/**
 * Created by sini on 10/25/14.
 */
public final class RSAKeygen {

    private static final int LINE_LENGTH = 60;
    private static final int RADIX = 16;

    public static void main(String[] args) throws Exception {

        // Grab the key pair generator for RSA
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048);

        // Generate the public and private key
        KeyPair kp = kpg.genKeyPair();

        // Get the specs about both keys
        KeyFactory fact = KeyFactory.getInstance("RSA");
        RSAPublicKeySpec publicSpec = fact.getKeySpec(kp.getPublic(), RSAPublicKeySpec.class);
        RSAPrivateKeySpec privateSpec = fact.getKeySpec(kp.getPrivate(), RSAPrivateKeySpec.class);

        // Generate the JSON object that holds all the information
        JsonObject obj = new JsonObject();
        obj.add("public_exp", toArray(publicSpec.getPublicExponent(), LINE_LENGTH));
        obj.add("private_exp", toArray(privateSpec.getPrivateExponent(), LINE_LENGTH));
        obj.add("modulus", toArray(publicSpec.getModulus(), LINE_LENGTH));

        try(FileWriter writer = new FileWriter("./data/keypair.json")) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            writer.write(gson.toJson(obj));
            writer.flush();
        }
    }

    private static JsonArray toArray(BigInteger bi, int lineLength) {
        if(lineLength < 1) {
            throw new IllegalArgumentException();
        }

        JsonArray array = new JsonArray();
        String str = bi.toString(RADIX);

        String current = "";
        int counter = 0;
        for(int i = 0; i < str.length(); i++) {
            if(counter++ > lineLength) {
                array.add(new JsonPrimitive(current));
                current = "";
                counter = 0;
            }
            current += str.charAt(i);
        }

        if(!current.equals("")) array.add(new JsonPrimitive(current));

        return array;
    }
}

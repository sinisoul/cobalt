package us.cobalt.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.StringTokenizer;
import java.util.stream.IntStream;

/**
 * Created by eve on 10/8/2014
 */
public final class StringUtils {

	private StringUtils() {
	}

	public static int addressToInt(String host) {
		try {
			InetAddress addr = InetAddress.getByName(host);
			byte[] octals = addr.getAddress();
			return IntStream.range(0, octals.length).reduce(0, (res, index) -> res = res << 8 | octals[index] & 0xFF);
		} catch (UnknownHostException e) {
			throw new RuntimeException("malformed host: " + host);
		}
	}

	/**
	 * Formats the specified {@code String}, capitalizes the first character and any other characters that follow an underscore <tt>_</tt>.
	 * 
	 * @param str The {@code String} to camelize.
	 * @return The camelized {@code String}.
	 * 
	 *         <p>
	 *         eg: <tt>hello_william => HelloWilliam</tt>
	 *         </p>
	 * 
	 *         <p>
	 *         eg: <tt>0123_hello => 0123Hello</tt>
	 *         </p>
	 */
	public static String camelize(String str) {
		StringBuilder bldr = new StringBuilder();
		StringTokenizer tokenizer = new StringTokenizer(str, "_");
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			String first = token.substring(0, 1);
			bldr.append(first.toUpperCase());
			if (str.length() > 1) {
				bldr.append(token.substring(1));
			}
		}
		return bldr.toString();
	}

	public static String pluralize(String str, int count) {
		if (count < 0) {
			throw new IllegalArgumentException("Count must be greater than or equal to zero");
		}

		if (count < 2) {
			return str;
		}

		char ending = str.toLowerCase().charAt(str.length() - 1);
		String suffix = "s";
		switch (ending) {
		case 'x':
			suffix = "es";
			break;
		}

		return str + suffix;
	}

}
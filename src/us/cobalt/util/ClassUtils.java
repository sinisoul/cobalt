package us.cobalt.util;

/**
 * Created by sini on 10/20/14.
 */
public final class ClassUtils {

	private ClassUtils() {
	}

	public static <T> String getClassName(T obj) {
		return obj.getClass().getSimpleName();
	}

}
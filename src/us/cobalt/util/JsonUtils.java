package us.cobalt.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

/**
 * Created by sini on 10/25/14.
 */
public final class JsonUtils {

    private JsonUtils() {}

    public static String parseStringArray(JsonArray arr) {
        String str = "";
        for(JsonElement element : arr) {
            str += element.getAsString();
        }
        return str;
    }
}

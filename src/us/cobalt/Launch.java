package us.cobalt;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonParser;

/**
 * Created by sini on 8/18/14.
 */
public final class Launch {

	private static final Logger logger = LoggerFactory.getLogger(Launch.class);

	private Launch() {
	}

	public static void main(String[] args) {
		// Setup the environment for the server
		Environment env = Environment.forType(captureArg(args, "env", "development"));
		parseConfig(env, args);

		// Override the configuration with the provided command line arguments
		env.parseArguments(args);

		Server server = new Server(env);
		server.loadServices();
		server.start();
	}

	private static String captureArg(String[] args, String name, String def) {
		for (String arg : args) {
			if (!arg.startsWith("--" + name)) {
				continue;
			}
			String str = arg.substring(name.length());
			if (!str.matches("=\\w+")) {
				throw new IllegalArgumentException("Illegally formatted --" + name + " argument");
			}
			return str.substring(1);
		}
		return def;
	}

	private static void parseConfig(Environment env, String[] args) {
		Path path = Paths.get(captureArg(args, "config", "data/config.json"));
		if (!Files.exists(path)) {
			logger.warn("Expected configuration file at " + path + ", but it did not exist!");
			return;
		}
		try {
			env.parseJSON(new JsonParser().parse(new FileReader(path.toFile())).getAsJsonObject());
		} catch (IllegalStateException ex) {
			logger.error("Provided configuration file at " + path + " is not a JSON object!");
		} catch (FileNotFoundException ex) {
			logger.error("Configuration file does not exist!", ex);
		}
	}

}
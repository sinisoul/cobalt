package us.cobalt;

/**
 * Created by sini on 8/17/14.
 */
public final class ServerContext {

	private final Server server;

	public ServerContext(Server server) {
		this.server = server;
	}

	public <T extends Service> boolean serviceExists(Class<T> cls) {
		return server.serviceExists(cls);
	}

	public <T extends Service> T getService(Class<T> clazz) {
		return server.getService(clazz);
	}

	public Environment env() {
		return server.env();
	}

}
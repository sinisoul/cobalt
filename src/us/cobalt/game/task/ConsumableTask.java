package us.cobalt.game.task;

/**
 * Created by sini on 10/20/14.
 */
public final class ConsumableTask extends Task {

	private final TaskContext context = new TaskContext(this);
	private final TaskConsumer closure;

	public ConsumableTask(TaskConsumer closure, long delay) {
		this(closure, TimeUnit.TICKS, delay, false);
	}

	public ConsumableTask(TaskConsumer closure, TimeUnit unit, long delay, boolean immediate) {
		super(unit, delay, immediate);
		this.closure = closure;
	}

	@Override
	public void execute() {
		closure.accept(this);
	}

}
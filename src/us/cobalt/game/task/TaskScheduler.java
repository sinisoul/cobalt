package us.cobalt.game.task;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sini on 10/22/14.
 */
public final class TaskScheduler {

	private static final Logger logger = LoggerFactory.getLogger(TaskScheduler.class);

	public static final long PULSE_RATE = 50L;

	private final ExecutorService executor = Executors.newSingleThreadExecutor();
	private final Queue<Task> tasks = new ArrayDeque<>();

	public TaskScheduler() {
	}

	public void schedule(Task task) {
		synchronized (tasks) {
			tasks.offer(task);
		}
	}

	public void start() {
		executor.execute(() -> {
			for (;;) {

				// Stop execution if the thread is interrupted
				if (Thread.interrupted()) {
					break;
				}

				long start = System.currentTimeMillis();

				synchronized (tasks) {
					Iterator<Task> iterator = tasks.iterator();
					while (iterator.hasNext()) {
						Task task = iterator.next();

						try {
							task.pulse();
						} catch (Throwable reason) {
							logger.error("Unhandled exception caught during pulse", reason);
						}

						if (task.stopped()) {
							iterator.remove();
						}
					}
				}

				long delay = PULSE_RATE - System.currentTimeMillis() + start;
				if (delay > 0) {
					try {
						Thread.sleep(delay);
					} catch (InterruptedException ex) {
					}
				} else {
					logger.info("Scheduler being overloaded by " + Math.abs(delay) + " milliseconds");
				}
			}
		});
	}

	public void stop() {
		synchronized (tasks) {
			tasks.forEach(task -> task.stop());
		}
		executor.shutdown();
	}

}
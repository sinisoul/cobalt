package us.cobalt.game.task;

/**
 * Created by sini on 10/20/14.
 */
public final class TaskContext {

	private final Task task;

	public TaskContext(Task task) {
		this.task = task;
	}

	public void stop() {
		task.stop();
	}

}
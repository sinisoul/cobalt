package us.cobalt.game.task;

import static us.cobalt.game.task.TaskScheduler.PULSE_RATE;

/**
 * Created by sini on 10/22/14.
 */
public enum TimeUnit {

	SECONDS(1000L), TICKS(600L), PULSES(PULSE_RATE);

	private final long pulses;

	TimeUnit(long timeMilliseconds) {
		if (timeMilliseconds < PULSE_RATE) {
			throw new IllegalArgumentException("Time provided must be greater than the scheduler cycle rate");
		}
		pulses = timeMilliseconds / PULSE_RATE;
	}

	public long convertTo(long sourceDuration, TimeUnit unit) {
		return sourceDuration * pulses / unit.pulses;
	}

}
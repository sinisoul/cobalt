package us.cobalt.game.task;

/**
 * Created by sini on 10/22/14.
 */
public abstract class Task {

	private final int delay;
	private int count;
	private boolean stopped;

	public Task(TimeUnit unit, long del, boolean immediate) {
		delay = (int) unit.convertTo(del, TimeUnit.PULSES);
		count = immediate ? 0 : delay;
	}

	public void pulse() {
		if (count-- == 0) {
			execute();
			count = delay;
		}
	}

	public abstract void execute();

	public void stop() {
		stopped = true;
	}

	public boolean stopped() {
		return stopped;
	}

}
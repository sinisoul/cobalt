package us.cobalt.game.task;

import java.util.function.Consumer;

/**
 * Created by sini on 10/20/14.
 */
public interface TaskConsumer extends Consumer<Task> {

	/**
	 * Executes the closure
	 */
	@Override
	void accept(Task task);

}
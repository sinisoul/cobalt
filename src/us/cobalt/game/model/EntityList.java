package us.cobalt.game.model;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * Created by eve on 10/8/2014
 */
public final class EntityList<T extends Entity> {

	private final Entity[] arr;
	private final int capacity;
	private final Queue<Integer> removed = new ArrayDeque<>();
	private int size = 0;

	public EntityList(int capacity) {
		arr = new Entity[capacity];
		this.capacity = capacity;
	}

	public boolean add(T entity) {

		// Check that we haven't reached capacity yet
		if (size >= capacity) {
			return false;
		}

		// Add the entity to the list
		int slot = nextSlot();
		arr[slot] = entity;
		entity.setId(slot);
		size++;

		return true;
	}

	public void remove(T entity) {

		// Check that the entity actually belongs to a list
		assert entity.id() != -1;

		// Remove the entity from the list
		arr[entity.id()] = null;
		removed.add(entity.id());
		entity.setId(-1);
		size--;
	}

	public int size() {
		return size;
	}

	private int nextSlot() {
		if (!removed.isEmpty()) {
			return removed.poll();
		}
		return size;
	}

}
package us.cobalt.game.model;

/**
 * Created by sini on 9/29/14.
 */
public final class World {

	private final EntityList<Player> players = new EntityList<>(2048);

	public boolean addPlayer(Player plr) {

		// Attempt to add the player to the world
		if (!players.add(plr)) {
			return false;
		}
		return true;
	}

}
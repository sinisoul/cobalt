package us.cobalt.game.model;

/**
 * Created by sini on 9/29/14.
 */
public abstract class Mob extends Entity {

	private boolean routeClip;

	public void routeClip(boolean routeClip) {
		this.routeClip = routeClip;
	}

	public boolean routeClip() {
		return routeClip;
	}

}
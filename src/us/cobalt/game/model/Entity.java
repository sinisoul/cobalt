package us.cobalt.game.model;

/**
 * Created by sini on 9/29/14.
 */
public abstract class Entity {

	private Position position = new Position(0, 0, 0);
	private int id = -1;

	public void setId(int id) {
		this.id = id;
	}

	public int id() {
		return id;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

}
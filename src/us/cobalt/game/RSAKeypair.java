package us.cobalt.game;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import us.cobalt.util.JsonUtils;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by sini on 10/25/14.
 */
public final class RSAKeypair {

    private final BigInteger publicExponent;
    private final BigInteger privateExponent;
    private final BigInteger modulus;

    public RSAKeypair(BigInteger publicExponent, BigInteger privateExponent, BigInteger modulus) {
        this.publicExponent = publicExponent;
        this.privateExponent = privateExponent;
        this.modulus = modulus;
    }

    public static RSAKeypair loadFromJSON(Path path) throws IOException {
        return loadJSON(new JsonParser().parse(Files.newBufferedReader(path)).getAsJsonObject());
    }

    public static RSAKeypair loadJSON(JsonObject obj) {
        String publicExp = JsonUtils.parseStringArray(obj.getAsJsonArray("public_exp"));
        String privateExp = JsonUtils.parseStringArray(obj.getAsJsonArray("private_exp"));
        String modulus = JsonUtils.parseStringArray(obj.getAsJsonArray("modulus"));
        return new RSAKeypair(new BigInteger(publicExp, 16),
                              new BigInteger(privateExp, 16),
                              new BigInteger(modulus, 16));
    }
}

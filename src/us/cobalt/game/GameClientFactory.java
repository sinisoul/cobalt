package us.cobalt.game;

import us.cobalt.game.net.Connection;
import us.cobalt.game.net.msg.GameMessage;

/**
 * Created by sini on 10/23/14.
 */
public interface GameClientFactory<T extends GameMessage> {

	GameClient create(Connection conn, T message);

}
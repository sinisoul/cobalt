package us.cobalt.game;

import static us.cobalt.game.ResponseStatus.STATUS_CONNECTION_LIMIT_EXCEEDED;
import static us.cobalt.game.ResponseStatus.STATUS_FULL;
import static us.cobalt.game.ResponseStatus.STATUS_OKAY;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.cobalt.Service;
import us.cobalt.game.net.msg.impl.ServiceStatusMessage;
import us.cobalt.game.task.ConsumableTask;
import us.cobalt.game.task.Task;
import us.cobalt.game.task.TaskConsumer;
import us.cobalt.game.task.TaskScheduler;
import us.cobalt.game.task.TimeUnit;
import us.cobalt.util.StringUtils;

import com.google.common.collect.ConcurrentHashMultiset;
import com.google.common.collect.Multiset;

/**
 * Created by sini on 10/20/14.
 */
public abstract class GameService<T extends GameClient> extends Service {

	/**
	 * The logger for game service instances.
	 */
	private static final Logger logger = LoggerFactory.getLogger(GameService.class);

	/**
	 * The task scheduler for the service.
	 */
	private final TaskScheduler scheduler = new TaskScheduler();

	/**
	 * The queue of clients to attempt to register to the service.
	 */
	private final Queue<T> clientsToRegister = new ArrayDeque<>();

	/**
	 * The queue of clients to remove from the service.
	 */
	private final Queue<T> clientsToRemove = new ArrayDeque<>();

	/**
	 * The list of clients for the service.
	 */
	private final ClientList<T> clients = new ClientList<>();

	/**
	 * The amount of clients that this service is limited to. If you set this limit to lower than the previous limit was and there are more connections that the set limit than as connections are closed they will fall below the limit and new client connections will be accepted.
	 */
	private int clientLimit = 1;

	/**
	 * List of client consumers which act as handlers.
	 */
	private final List<ClientHandler<T>> handlers = new ArrayList<>();

	/**
	 * Multiset of all registered ips.
	 */
	private final Multiset<Integer> registeredIps = ConcurrentHashMultiset.create();

	/**
	 * The amount of connections to limit each IP to.
	 */
	private int connectionLimit = 1;

	/**
	 * Constructs a new {@link us.cobalt.game.GameService};
	 */
	public GameService() {
	}

	/**
	 * Loads the service.
	 */
	public abstract void load();

	/**
	 * Initializes the service.
	 */
	@Override
	public final void init() {
		schedule(task -> {
			synchronized (clientsToRegister) {
				for (int i = 0; i < 50; i++) {

					// Poll the next client to register to the service
					T client = clientsToRegister.poll();
					if (client == null) {
						break;
					}

					// Register the client to the service and write back the status
					client.write(new ServiceStatusMessage(attemptRegister(client))).flush();
				}
			}

			synchronized (clientsToRemove) {
				T client;
				while ((client = clientsToRemove.poll()) != null) {
					removeConnectionThrottle(client);
					clients.remove(client);
				}
			}

			// Before we handle each of the client dispatch the queued messages stored from reading.
			clients.forEach(client -> client.dispatchMessages());

			// Handle each of the clients with the provided consumers
			clients.forEach(client -> handlers.forEach(consumer -> consumer.accept(client)));
		});

		// When the service starts, kick the scheduler into action
		onStart(() -> scheduler.start());

		// Load the service
		load();
	}

	/**
	 * Limit the amount of clients that are allowed to be registered to the service.
	 *
	 * @param amount The amount of clients.
	 */
	public void limitClients(int amount) {
		clientLimit = amount;
	}

	/**
	 * Limit the amount of connections for each ip allowed to connect to the service.
	 *
	 * @param amount The amount of connections.
	 */
	public void limitConnections(int amount) {
		connectionLimit = amount;
	}

	/**
	 * Schedules a new task.
	 *
	 * @param consumer The consumer function.
	 */
	public void schedule(TaskConsumer consumer) {
		schedule(consumer, TimeUnit.PULSES, 1L);
	}

	/**
	 * Schedules a new task.
	 *
	 * @param consumer The consumer function.
	 * @param unit The time unit.
	 * @param delay The delay between executions.
	 */
	public void schedule(TaskConsumer consumer, TimeUnit unit, long delay) {
		schedule(consumer, unit, delay, false);
	}

	/**
	 * Schedules a new task.
	 *
	 * @param consumer The consumer function.
	 * @param unit The time unit.
	 * @param delay The delay between executions.
	 * @param immediate Flag for if the task should execute immediately.
	 */
	public void schedule(TaskConsumer consumer, TimeUnit unit, long delay, boolean immediate) {
		schedule(new ConsumableTask(consumer, unit, delay, immediate));
	}

	/**
	 * Schedules a new task.
	 *
	 * @param task The task to schedule.
	 */
	public void schedule(Task task) {
		scheduler.schedule(task);
	}

	/**
	 * Appends a client handler.
	 *
	 * @param handler The handler.
	 */
	public void handler(ClientHandler<T> handler) {
		handlers.add(handler);
	}

	/**
	 * Registers a client to the service.
	 *
	 * @param client The client to register.
	 */
	public void register(T client) {
		synchronized (clientsToRegister) {
			clientsToRegister.offer(client);
		}
	}

	/**
	 * Schedules a client to be unregistered.
	 *
	 * @param client The client to unregister.
	 */
	public void unregister(T client) {
		synchronized (clientsToRemove) {
			clientsToRemove.offer(client);
		}
	}

	/**
	 * Attempts to register a client to the service.
	 *
	 * @param client The client.
	 * @return The registration status.
	 */
	private int attemptRegister(T client) {

		// Check if the connection needs to be throttled
		if (checkThrottleConnection(client)) {
			return STATUS_CONNECTION_LIMIT_EXCEEDED;
		}

		// Check if the service is full and if it is then we can't register the client!
		if (clients.size() >= clientLimit) {
			return STATUS_FULL;
		}

		// Add the client to the service and release access to the clients.
		clients.add(client);

		return STATUS_OKAY;
	}

	/**
	 * Checks if we need to throttle the connection.
	 *
	 * @param client The client.
	 * @return If the incoming connection needs to be throttled.
	 */
	private boolean checkThrottleConnection(GameClient client) {
		int address = StringUtils.addressToInt(client.hostName());
		if (registeredIps.count(address) >= connectionLimit) {
			return true;
		}
		registeredIps.add(address, 1);
		return false;
	}

	/**
	 * Removes a registered instance of a connection from the IP table.
	 *
	 * @param client The client to decrement the connection throttle for.
	 */
	private void removeConnectionThrottle(GameClient client) {
		int address = StringUtils.addressToInt(client.hostName());
		registeredIps.remove(address, 1);
	}
}
package us.cobalt.game;

import us.cobalt.game.net.Connection;
import us.cobalt.game.net.msg.GameMessage;
import us.cobalt.game.net.msg.MessageDispatcher;

/**
 * Created by sini on 10/20/14.
 */
public abstract class GameClient {

	private final Connection conn;
	private boolean inactive;
	private int id = -1;

	public GameClient(Connection conn) {
		this.conn = conn;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int id() {
		return id;
	}

	public void dispatchMessages() {
		MessageDispatcher dispatcher = conn.getDispatcher();
		if (!dispatcher.immediatelyDispatch()) {
			dispatcher.dispatch();
		}
	}

	public GameClient writeAndFlush(GameMessage message) {
		write(message).flush();
		return this;
	}

	public GameClient write(GameMessage message) {
		conn.write(message);
		return this;
	}

	public String hostName() {
		return conn.remoteAddress().getHostString();
	}

	public boolean inactive() {
		// If the session is still active and the connection closes
		// then destroy the session and note the session as inactive.
		if (!inactive && conn.closed()) {
			destroy();
		}
		return inactive;
	}

	public void flush() {
		conn.flush();
	}

	public void destroy() {
		synchronized (this) {
			if (inactive) {
				return;
			}
			conn.destroy();
			inactive = true;
		}
	}

}
package us.cobalt.game.worldlist;

/**
 * Created by eve on 10/16/2014
 */
public final class Location {

	public static final int FLAG_AUSTRALIA = 16;
	public static final int FLAG_BELGIUM = 22;
	public static final int FLAG_BRAZIL = 31;
	public static final int FLAG_CANADA = 38;
	public static final int FLAG_DENMARK = 58;
	public static final int FLAG_FINLAND = 69;
	public static final int FLAG_UK = 77;
	public static final int FLAG_IRELAND = 101;
	public static final int FLAG_MEXICO = 152;
	public static final int FLAG_NETHERLANDS = 161;
	public static final int FLAG_NORWAY = 162;
	public static final int FLAG_SWEDEN = 191;
	public static final int FLAG_USA = 225;

	private final String id;
	private final String name;
	private final int flag;

	public Location(String id, String name, int flag) {
		this.id = id;
		this.flag = flag;
		this.name = name;
	}

	public String id() {
		return id;
	}

	public int flag() {
		return flag;
	}

	public String name() {
		return name;
	}

	public static int getFlagForName(String name) {
		switch (name) {
		case "australia":
			return FLAG_AUSTRALIA;
		case "belgium":
			return FLAG_BELGIUM;
		case "brazil":
			return FLAG_BRAZIL;
		case "canada":
			return FLAG_CANADA;
		case "denmark":
			return FLAG_DENMARK;
		case "finland":
			return FLAG_FINLAND;
		case "uk":
			return FLAG_UK;
		case "ireland":
			return FLAG_IRELAND;
		case "mexico":
			return FLAG_MEXICO;
		case "netherlands":
			return FLAG_NETHERLANDS;
		case "norway":
			return FLAG_NORWAY;
		case "sweden":
			return FLAG_SWEDEN;
		case "usa":
			return FLAG_USA;
		default:
			return -1;
		}
	}

}
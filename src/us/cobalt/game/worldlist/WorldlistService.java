package us.cobalt.game.worldlist;

import static us.cobalt.Environment.DEVELOPMENT_ENV;
import static us.cobalt.Environment.TESTING_ENV;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.cobalt.InitializationException;
import us.cobalt.game.GameService;
import us.cobalt.game.net.msg.impl.WorldlistMessage;

/**
 * Created by sini on 10/23/14.
 */
public final class WorldlistService extends GameService<WorldlistClient> {

	private static final Logger logger = LoggerFactory.getLogger(WorldlistService.class);
	private final WorldList worlds = new WorldList();
	private final LocationList locations = new LocationList();

	public WorldlistService() {
	}

	@Override
	public void load() {
		// In the development and testing environment just load all of the data manually
		if (inEnvironment(DEVELOPMENT_ENV, TESTING_ENV)) {

			// Load all of the world locations
			loadLocations(Paths.get(configDir(), "worldlist/locations.json"));

			// Load all of the worlds
			loadWorlds(Paths.get(configDir(), "worldlist/worlds.json"));
		}

		// Limit the amount of requests to accept
		limitClients(getInt("request_limit", 50));

		// For all of the clients that connect just write out the world list
		// and then unregister the client from the service. Simple? Very.
		handler(client -> {
			client.writeAndFlush(new WorldlistMessage(worlds, locations));
			unregister(client);
		});
	}

	public void loadLocations(Path path) {
		try {
			logger.info("Loading world locations from disk");
			locations.parseJSON(path);
		} catch (FileNotFoundException reason) {
			logger.error("Failed to load the world list locations", reason);
			throw new InitializationException(reason);
		}
	}

	public void loadWorlds(Path path) {
		try {
			logger.info("Loading worlds from disk");
			worlds.parseJSON(path);
		} catch (FileNotFoundException reason) {
			logger.error("Failed to load the world list", reason);
			throw new InitializationException(reason);
		}
	}

}
package us.cobalt.game.worldlist;

public final class World {

	public static final int FLAG_MEMBERS = 0x1;
	public static final int FLAG_QUICKCHAT = 0x2;
	public static final int FLAG_PVP = 0x4;
	public static final int FLAG_LOOTSHARE = 0x8;
	public static final int FLAG_HIGHLIGHT = 0x10;

	private final int id;
	private final String location;
	private final String activity;
	private final String address;
	private int playerCount;
	private int flags;

	public World(int id, String location, String activity, String address) {
		this.id = id;
		this.location = location;
		this.activity = activity;
		this.address = address;
	}

	public int id() {
		return id;
	}

	public int flags() {
		return flags;
	}

	public String location() {
		return location;
	}

	public String activity() {
		return activity;
	}

	public String ipAddress() {
		return address;
	}

	public int playerCount() {
		return playerCount;
	}

	public void setFlag(int flag) {
		flags |= flag;
	}

	public boolean isFlagActive(int flag) {
		return (flags & flag) != 0;
	}

	public boolean isFlagInactive(int flag) {
		return (flags & flag) == 0;
	}

	public void unsetFlag(int flag) {
		flags &= 0xFFFF - flag;
	}

	public static int getFlagForName(String name) {
		switch (name.toLowerCase()) {
		case "members":
			return FLAG_MEMBERS;
		case "quickchat":
			return FLAG_QUICKCHAT;
		case "pvp":
			return FLAG_PVP;
		case "lootshare":
			return FLAG_LOOTSHARE;
		case "highlight":
			return FLAG_HIGHLIGHT;
		default:
			return -1;
		}
	}

}
package us.cobalt.game.worldlist;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Path;

import us.cobalt.game.net.ByteBuffer;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Created by eve on 10/16/2014
 */
public final class LocationList {

	private final Location[] locations;
	private final int capacity;
	private int size;

	public LocationList() {
		this(10);
	}

	public LocationList(int capacity) {
		locations = new Location[capacity];
		this.capacity = capacity;
	}

	private int nextSlot() {
		for (int i = 0; i < capacity; i++) {
			if (locations[i] != null) {
				continue;
			}
			return i;
		}
		return -1;
	}

	public boolean add(Location location) {
		int slot = nextSlot();
		if (slot != -1) {
			locations[slot] = location;
			size++;
		}
		return slot != -1;
	}

	public int indexOf(String name) {
		for (int i = 0; i < capacity; i++) {
			if (locations[i] != null) {
				if (name.equals(locations[i].id())) {
					return i;
				}
			}
		}
		return -1;
	}

	public int size() {
		return size;
	}

	public void encode(ByteBuffer buf) {
		buf.putSmart(size);
		for (Location location : locations) {
			if (location != null) {
				buf.putSmart(location.flag());
				buf.putJagstr2(location.name());
			}
		}
	}

	public void parseJSON(Path path) throws FileNotFoundException {
		parseJSON(new JsonParser().parse(new FileReader(path.toFile())).getAsJsonArray());
	}

	public void parseJSON(JsonArray array) {
		array.forEach(element -> {
			// Get the element as a JSON object
			JsonObject obj = element.getAsJsonObject();

			String id = obj.get("id").getAsString();
			int flag = Location.getFlagForName(obj.get("flag").getAsString());
			String name = obj.get("name").getAsString();

			// Put the location into the list
			add(new Location(id, name, flag));
		});
	}
}
package us.cobalt.game.worldlist;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Path;
import java.util.TreeMap;

import us.cobalt.game.net.ByteBuffer;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Created by eve
 *
 * TODO: Player count/World revisions to save on bandwidth
 */
public final class WorldList {

	private final TreeMap<Integer, World> worlds = new TreeMap<>();

	public WorldList() {
	}

	private void checkId(int id) {
		if (id < 0 || id >= 32767) {
			throw new ArrayIndexOutOfBoundsException("Id is out of bounds");
		}
	}

	public void add(World world) {
		checkId(world.id());
		worlds.put(world.id(), world);
	}

	public World get(int id) {
		checkId(id);
		return worlds.get(id);
	}

	public boolean empty() {
		return worlds.isEmpty();
	}

	public int size() {
		return worlds.size();
	}

	public void encode(LocationList locations, ByteBuffer buf) {
		int minId = worlds.firstKey();
		int maxId = minId;

		for (World world : worlds.values()) {
			maxId = Math.max(maxId, world.id());
		}

		buf.putSmart(minId);
		buf.putSmart(maxId);
		buf.putSmart(size());

		for (World world : worlds.values()) {
			buf.putSmart(world.id() - minId);
			buf.putByte(locations.indexOf(world.location()));
			buf.putInt(world.flags());
			buf.putJagstr2(world.activity());
			buf.putJagstr2(world.ipAddress());
		}

		buf.putInt(0xDEADBEEF);

		for (World world : worlds.values()) {
			buf.putSmart(world.id() - minId);
			buf.putShort(world.playerCount());
		}
	}

	public void parseJSON(Path path) throws FileNotFoundException {
		parseJSON(new JsonParser().parse(new FileReader(path.toFile())).getAsJsonArray());
	}

	public void parseJSON(JsonArray array) {
		array.forEach(element -> {
			// Get the element as a JSON object
			JsonObject obj = element.getAsJsonObject();

			int id = obj.get("id").getAsInt();
			String loc = obj.get("location").getAsString();
			JsonArray flags = obj.getAsJsonArray("flags");
			String address = obj.get("address").getAsString();
			String activity = obj.has("activity") ? obj.get("activity").getAsString() : "";

			// Create the world type with the specified id and id
			World world = new World(id, loc, activity, address);

			// Set all of the worlds flags
			flags.forEach(flag -> world.setFlag(World.getFlagForName(flag.getAsString())));

			// Add the world type to the list
			add(world);
		});
	}

}
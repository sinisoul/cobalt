package us.cobalt.game.net.msg;

import us.cobalt.game.net.ByteBuffer;
import us.cobalt.game.net.frame.GameFrame;

/**
 * Created by eve on 10/16/2014
 */
public abstract class MessageDecoder<T extends GameMessage> {

	private final Class<T> messageClass;

	public MessageDecoder(Class<T> messageClass) {
		this.messageClass = messageClass;
	}

	public abstract T decode(GameFrame frame, ByteBuffer input);

	public Class<T> getMessageClass() {
		return messageClass;
	}

}
package us.cobalt.game.net.msg;

import us.cobalt.game.net.Connection;

/**
 * Created by sini on 10/20/14.
 */
public final class ConnectionMessageDispatcher extends MessageDispatcher {

	private final MessageHandlerSet<ConnectionMessageHandler> handlers;
	private final Connection conn;

	public ConnectionMessageDispatcher(Connection conn, MessageHandlerSet handlers) {
		super(true);
		this.conn = conn;
		this.handlers = handlers;
	}

	@Override
	public void dispatch(GameMessage message) {
        ConnectionMessageHandler handler = handlers.getHandler(message);
        handler.handle(conn, message);
    }
}
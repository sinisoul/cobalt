package us.cobalt.game.net.msg;

/**
 * Created by sini on 10/19/14.
 */
public final class CodecNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -8210931126943489771L;

	public CodecNotFoundException() {
		super();
	}

	public CodecNotFoundException(String message) {
		super(message);
	}

	public CodecNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public CodecNotFoundException(Throwable cause) {
		super(cause);
	}

}
package us.cobalt.game.net.msg.handler;

import us.cobalt.game.GameClientFactory;
import us.cobalt.game.GameService;
import us.cobalt.game.net.Connection;
import us.cobalt.game.net.msg.ConnectionMessageHandler;
import us.cobalt.game.net.msg.GameMessage;

/**
 * Created by sini on 10/20/14.
 */
public final class GameServiceHandshakeHandler<T extends GameMessage> extends ConnectionMessageHandler<T> {

	private final GameClientFactory<T> factory;
	private final GameService service;

	public GameServiceHandshakeHandler(Class<T> cls, GameService service, GameClientFactory<T> factory) {
		super(cls);
		this.factory = factory;
		this.service = service;
	}

	@Override
	public void handle(Connection conn, T message) {
		service.register(factory.create(conn, message));
	}

}
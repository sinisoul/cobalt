package us.cobalt.game.net.msg.handler;

import us.cobalt.game.net.Connection;
import us.cobalt.game.net.msg.ConnectionMessageHandler;
import us.cobalt.game.net.msg.impl.CheckShardActiveMessage;

/**
 * Created by sini on 10/24/14.
 */
public final class CheckShardActiveHandler extends ConnectionMessageHandler<CheckShardActiveMessage> {

    public CheckShardActiveHandler() {
        super(CheckShardActiveMessage.class);
    }

    @Override
    public void handle(Connection conn, CheckShardActiveMessage message) {

    }
}

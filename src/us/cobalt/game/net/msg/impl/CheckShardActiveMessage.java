package us.cobalt.game.net.msg.impl;

import us.cobalt.game.net.msg.GameMessage;

/**
 * Created by sini on 10/24/14.
 */
public final class CheckShardActiveMessage extends GameMessage {

    private final int shard;

    public CheckShardActiveMessage(int shard) {
        this.shard = shard;
    }

    public int getShard() {
        return shard;
    }
}

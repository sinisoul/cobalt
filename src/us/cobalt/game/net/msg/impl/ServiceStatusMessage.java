package us.cobalt.game.net.msg.impl;

import us.cobalt.game.net.msg.GameMessage;

/**
 * Created by sini on 10/20/14.
 */
public final class ServiceStatusMessage extends GameMessage {

	private final int status;

	public ServiceStatusMessage(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}

}
package us.cobalt.game.net.msg.impl;

import us.cobalt.game.net.msg.GameMessage;

/**
 * Created by eve on 10/16/2014
 */
public final class RefreshWorldlistMessage extends GameMessage {

	private final int revision;

	public RefreshWorldlistMessage(int revision) {
		this.revision = revision;
	}

	public int getRevision() {
		return revision;
	}

}
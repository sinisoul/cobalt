package us.cobalt.game.net.msg.impl;

import us.cobalt.game.net.msg.GameMessage;
import us.cobalt.game.worldlist.LocationList;
import us.cobalt.game.worldlist.WorldList;

/**
 * Created by sini on 10/20/14.
 */
public final class WorldlistMessage extends GameMessage {

	private final WorldList worlds;
	private final LocationList locations;

	public WorldlistMessage(WorldList worlds, LocationList locations) {
		this.worlds = worlds;
		this.locations = locations;
	}

	public LocationList getLocations() {
		return locations;
	}

	public WorldList getWorlds() {
		return worlds;
	}

}
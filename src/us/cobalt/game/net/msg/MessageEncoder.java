package us.cobalt.game.net.msg;

import us.cobalt.game.net.ByteBuffer;

/**
 * Created by eve on 10/16/2014
 */
public abstract class MessageEncoder<T extends GameMessage> {

	private final Class<T> messageClass;

	public MessageEncoder(Class<T> messageClass) {
		this.messageClass = messageClass;
	}

	public abstract void encode(T message, ByteBuffer output);

	public Class<T> getMessageClass() {
		return messageClass;
	}

}
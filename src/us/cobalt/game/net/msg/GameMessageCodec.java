package us.cobalt.game.net.msg;

import java.util.HashMap;
import java.util.Map;

import us.cobalt.game.net.ByteBuffer;
import us.cobalt.game.net.frame.GameFrame;
import us.cobalt.game.net.frame.GameFrameMetaSet;

/**
 * Created by sini on 10/19/14.
 */
public class GameMessageCodec {

	private final Map<Class<? extends GameMessage>, MessageDecoder<?>> decoders = new HashMap<>();
	private final Map<Class<? extends GameMessage>, MessageEncoder<?>> encoders = new HashMap<>();
	private final GameFrameMetaSet frames;

	public GameMessageCodec(GameFrameMetaSet frames) {
		this.frames = frames;
	}

	private void checkDecoderExists(GameFrame frame) {
		if (!decoderExists(frame)) {
			throw new CodecNotFoundException("No such decoder exists for frame " + frame.name());
		}
	}

	private void checkEncoderExists(GameMessage message) {
		if (!encoderExists(message)) {
			throw new CodecNotFoundException("No such encoder exists for message" + message.getClass());
		}
	}

	public void append(MessageDecoder<?> decoder) {
		decoders.put(decoder.getMessageClass(), decoder);
	}

	public void append(MessageEncoder<?> encoder) {
		encoders.put(encoder.getMessageClass(), encoder);
	}

	public boolean decoderExists(GameFrame frame) {
		return decoders.containsKey(frame.messageClass());
	}

	public boolean encoderExists(GameMessage message) {
		return encoders.containsKey(message.getClass());
	}

	public GameMessage decode(GameFrame frame) throws CodecNotFoundException {
		checkDecoderExists(frame);
		MessageDecoder<?> decoder = decoders.get(frame.messageClass());
		return decoder.decode(frame, new ByteBuffer(frame.bytes()));
	}

	@SuppressWarnings("unchecked")
	public GameFrame encode(GameMessage message) throws CodecNotFoundException {
		checkEncoderExists(message);

		// Encode the message to a buffer
		MessageEncoder<GameMessage> encoder = (MessageEncoder<GameMessage>) encoders.get(message.getClass());
		ByteBuffer buffer = new ByteBuffer();
		encoder.encode(message, buffer);

		// Copy the bytes from the encoded buffer
		int length = buffer.offset();
		byte[] bytes = new byte[length];
		buffer.offset(0).get(bytes);

		return new GameFrame(frames.forClass(message.getClass()), bytes);
	}

}
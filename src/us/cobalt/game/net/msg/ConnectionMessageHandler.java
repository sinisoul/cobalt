package us.cobalt.game.net.msg;

import us.cobalt.game.net.Connection;

/**
 * Created by sini on 10/20/14.
 */
public abstract class ConnectionMessageHandler<T extends GameMessage> extends MessageHandler<T> {

	public ConnectionMessageHandler(Class<T> cls) {
		super(cls);
	}

	public abstract void handle(Connection conn, T message);
}
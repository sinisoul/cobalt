package us.cobalt.game.net.msg;

/**
 * Created by sini on 10/20/14.
 */
public class MessageNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -4269744533693988659L;

	public MessageNotFoundException() {
		super();
	}

	public MessageNotFoundException(String message) {
		super(message);
	}

	public MessageNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public MessageNotFoundException(Throwable cause) {
		super(cause);
	}

}
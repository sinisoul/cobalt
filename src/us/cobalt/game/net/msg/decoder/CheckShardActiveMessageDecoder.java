package us.cobalt.game.net.msg.decoder;

import us.cobalt.game.net.ByteBuffer;
import us.cobalt.game.net.frame.GameFrame;
import us.cobalt.game.net.msg.MessageDecoder;
import us.cobalt.game.net.msg.impl.CheckShardActiveMessage;

/**
 * Created by sini on 10/24/14.
 */
public class CheckShardActiveMessageDecoder extends MessageDecoder<CheckShardActiveMessage> {

    public CheckShardActiveMessageDecoder() {
        super(CheckShardActiveMessage.class);
    }

    @Override
    public CheckShardActiveMessage decode(GameFrame frame, ByteBuffer input) {
        return new CheckShardActiveMessage(input.getUByte());
    }
}

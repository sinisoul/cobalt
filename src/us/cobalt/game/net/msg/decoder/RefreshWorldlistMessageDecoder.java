package us.cobalt.game.net.msg.decoder;

import us.cobalt.game.net.ByteBuffer;
import us.cobalt.game.net.frame.GameFrame;
import us.cobalt.game.net.msg.MessageDecoder;
import us.cobalt.game.net.msg.impl.RefreshWorldlistMessage;

/**
 * Created by eve on 10/17/2014
 */
public final class RefreshWorldlistMessageDecoder extends MessageDecoder<RefreshWorldlistMessage> {

	public RefreshWorldlistMessageDecoder() {
		super(RefreshWorldlistMessage.class);
	}

	@Override
	public RefreshWorldlistMessage decode(GameFrame frame, ByteBuffer input) {
		return new RefreshWorldlistMessage(input.getInt());
	}

}
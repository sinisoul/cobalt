package us.cobalt.game.net.msg;

/**
 * Created by sini on 10/24/14.
 */
public abstract class MessageHandler<T extends GameMessage> {

    private final Class<T> cls;

    protected MessageHandler(Class<T> cls) {
        this.cls = cls;
    }

    public Class<T> getMessageClass() {
        return cls;
    }
}

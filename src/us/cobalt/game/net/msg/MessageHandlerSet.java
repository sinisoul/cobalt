package us.cobalt.game.net.msg;

import java.util.HashMap;
import java.util.Map;

import us.cobalt.game.net.Connection;
import us.cobalt.util.ClassUtils;

/**
 * Created by sini on 10/20/14.
 */
public final class MessageHandlerSet<T extends MessageHandler> {

	private final Map<Class<? extends GameMessage>, T> handlers = new HashMap<>();

	public MessageHandlerSet() {}

	private void checkHandlerExists(GameMessage message) {
		if (!exists(message)) {
			throw new HandlerNotFoundException("No handler exists for " + ClassUtils.getClassName(message));
		}
	}

	@SuppressWarnings("unchecked")
	public void append(T handler) {
		handlers.put(handler.getMessageClass(), handler);
	}

	public boolean exists(GameMessage message) {
		return handlers.containsKey(message.getClass());
	}

	@SuppressWarnings("unchecked")
	public T getHandler(GameMessage message) {
        checkHandlerExists(message);
		return handlers.get(message.getClass());
	}
}
package us.cobalt.game.net.msg;

/**
 * Created by sini on 10/20/14.
 */
public final class HandlerNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 3788963654848453891L;

	public HandlerNotFoundException() {
		super();
	}

	public HandlerNotFoundException(String message) {
		super(message);
	}

	public HandlerNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public HandlerNotFoundException(Throwable cause) {
		super(cause);
	}

}
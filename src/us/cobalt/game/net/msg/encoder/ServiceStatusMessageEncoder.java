package us.cobalt.game.net.msg.encoder;

import us.cobalt.game.net.ByteBuffer;
import us.cobalt.game.net.msg.MessageEncoder;
import us.cobalt.game.net.msg.impl.ServiceStatusMessage;

/**
 * Created by sini on 10/23/14.
 */
public final class ServiceStatusMessageEncoder extends MessageEncoder<ServiceStatusMessage> {

	public ServiceStatusMessageEncoder() {
		super(ServiceStatusMessage.class);
	}

	@Override
	public void encode(ServiceStatusMessage msg, ByteBuffer output) {
		output.putByte(msg.getStatus());
	}

}
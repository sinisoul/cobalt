package us.cobalt.game.net.msg.encoder;

import us.cobalt.game.net.ByteBuffer;
import us.cobalt.game.net.msg.MessageEncoder;
import us.cobalt.game.net.msg.impl.WorldlistMessage;
import us.cobalt.game.worldlist.LocationList;
import us.cobalt.game.worldlist.WorldList;

/**
 * Created by sini on 10/20/14.
 */
public final class WorldlistMessageEncoder extends MessageEncoder<WorldlistMessage> {

	public WorldlistMessageEncoder() {
		super(WorldlistMessage.class);
	}

	@Override
	public void encode(WorldlistMessage msg, ByteBuffer output) {
		WorldList worlds = msg.getWorlds();

		output.putBoolean(true);
		output.putBoolean(true);

		LocationList locations = msg.getLocations();
		locations.encode(output);

		worlds.encode(locations, output);
	}
}

package us.cobalt.game.net.msg;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * Created by sini on 10/20/14.
 */
public abstract class MessageDispatcher {

	private final Queue<GameMessage> messages = new ArrayDeque<>();
	private final boolean immediatelyDispatch;

	public MessageDispatcher() {
		this(false);
	}

	public MessageDispatcher(boolean immediatelyDispatch) {
		this.immediatelyDispatch = immediatelyDispatch;
	}

	public void queue(GameMessage message) {
		synchronized (this) {
			if (!immediatelyDispatch) {
				messages.offer(message);
			} else {
				dispatch(message);
			}
		}
	}

	public void dispatch() {
		synchronized (this) {
			GameMessage message;
			while ((message = messages.poll()) != null) {
				dispatch(message);
			}
		}
	}

	public boolean immediatelyDispatch() {
		return immediatelyDispatch;
	}

	public abstract void dispatch(GameMessage message);

}
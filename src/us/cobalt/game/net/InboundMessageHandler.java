package us.cobalt.game.net;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.cobalt.game.net.msg.GameMessage;

/**
 * Created by eve on 10/15/2014
 */
public class InboundMessageHandler extends SimpleChannelInboundHandler<GameMessage> {

	private final Logger logger = LoggerFactory.getLogger(InboundMessageHandler.class);

	private final Connection connection;

	public InboundMessageHandler(Connection connection) {
		this.connection = connection;
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		logger.error("Uncaught exception reported, destroying connection", cause);
		connection.destroy();
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		logger.info("New channel active from {}", ctx.channel().remoteAddress());
	}

	@Override
	protected void channelRead0(ChannelHandlerContext channelHandlerContext, GameMessage msg) throws Exception {
		connection.queue(msg);
	}

}
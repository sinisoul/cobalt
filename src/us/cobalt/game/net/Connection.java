package us.cobalt.game.net;

import io.netty.channel.ChannelFutureListener;
import io.netty.channel.socket.SocketChannel;

import java.net.InetSocketAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.cobalt.game.net.msg.GameMessage;
import us.cobalt.game.net.msg.MessageDispatcher;
import us.cobalt.util.ISAACRandom;

/**
 * Created by eve on 10/14/2014
 */
public final class Connection {

	private static final Logger logger = LoggerFactory.getLogger(Connection.class);

	private final SocketChannel channel;
	private MessageDispatcher dispatcher;
	private ISAACRandom inputRandom;
	private ISAACRandom outputRandom;
	private boolean framesCiphered = false;
	private boolean closed = false;

	public Connection(SocketChannel channel) {
		this.channel = channel;
	}

	public void seedCiphers(int[] seed) {

		// Copy the seed to a new array
		int[] copy = new int[seed.length];
		System.arraycopy(seed, 0, copy, 0, seed.length);

		// Seed the input random
		inputRandom = new ISAACRandom(copy);

		// Increment each seed by 50
		for (int i = 0; i < 4; i++) {
			copy[i] += 50;
		}

		// Seed the output random
		outputRandom = new ISAACRandom(copy);

		// Note that the frame ids should be ciphered/deciphered
		framesCiphered = true;
	}

	public int decipherFrameId(int id) {
		return id - inputRandom.nextInt() & 0xff;
	}

	public int encipherFrameId(int id) {
		return id + outputRandom.nextInt() & 0xff;
	}

	public boolean isFramesCiphered() {
		return framesCiphered;
	}

	public InetSocketAddress remoteAddress() {
		return channel.remoteAddress();
	}

	public void setDispatcher(MessageDispatcher dispatcher) {
		this.dispatcher = dispatcher;
	}

	public MessageDispatcher getDispatcher() {
		return dispatcher;
	}

	public void queue(GameMessage message) {
		if (dispatcher != null) {
			dispatcher.queue(message);
		} else {
			logger.info("No dispatcher registered to connection");
		}
	}

	public Connection write(GameMessage message) {
		channel.write(message).addListener(ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE);
		return this;
	}

	public void flush() {
		channel.flush();
	}

	public void destroy() {
		synchronized (this) {
			if (closed) {
				return;
			}
			channel.close();
			closed = true;
		}
	}

	public boolean closed() {
		synchronized (this) {
			return closed;
		}
	}

}
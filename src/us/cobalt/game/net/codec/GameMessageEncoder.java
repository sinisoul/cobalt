package us.cobalt.game.net.codec;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

import us.cobalt.game.net.msg.GameMessage;
import us.cobalt.game.net.msg.GameMessageCodec;

/**
 * Created by sini on 10/19/14.
 */
public final class GameMessageEncoder extends MessageToMessageEncoder<GameMessage> {

	private final GameMessageCodec codec;

	public GameMessageEncoder(GameMessageCodec codec) {
		this.codec = codec;
	}

	@Override
	protected void encode(ChannelHandlerContext ctx, GameMessage msg, List<Object> list) throws Exception {
		list.add(codec.encode(msg));
	}

}
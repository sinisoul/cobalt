package us.cobalt.game.net.codec;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

import us.cobalt.game.net.frame.GameFrame;
import us.cobalt.game.net.obf.CodecNotFoundException;
import us.cobalt.game.net.obf.ObfCodecSet;

/**
 * Created by eve on 10/16/2014
 */
public final class GameFrameObfuscator extends MessageToMessageEncoder<GameFrame> {

	private final ObfCodecSet codecs;

	public GameFrameObfuscator(ObfCodecSet codecs) {
		this.codecs = codecs;
	}

	@Override
	protected void encode(ChannelHandlerContext ctx, GameFrame frame, List<Object> list) throws Exception {
		if (!frame.obfuscated()) {
			list.add(frame);
		} else {
			// Obfuscate the frame before proceeding any farther
			if (!codecs.exists(frame.id())) {
				throw new CodecNotFoundException("No such codec exists for obfuscating frame " + frame.name());
			}
			list.add(new GameFrame(frame.meta(), codecs.encode(frame.bytes(), frame.id())));
		}
	}

}
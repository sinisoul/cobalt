package us.cobalt.game.net.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import us.cobalt.game.net.Connection;
import us.cobalt.game.net.frame.GameFrame;

public final class GameFrameEncoder extends MessageToByteEncoder<GameFrame> {

	private final Connection conn;

	public GameFrameEncoder(Connection conn) {
		this.conn = conn;
	}

	@Override
	protected void encode(ChannelHandlerContext ctx, GameFrame frame, ByteBuf output) {

		if (frame.hasId()) {
			// Get the id of the frame we're writing out, if the frame is obfuscated then write out the obfuscated id
			int id = frame.obfuscated() ? frame.obf() : frame.id();

			// Write the id of the packet, if outgoing frames need to be ciphered then cipher
			// the id of the frame before writing it out
			output.writeByte(conn.isFramesCiphered() ? conn.encipherFrameId(id) : id);
		}

		// Write out the length of the frame if is either var byte or var short sized
		if (frame.isVarByteSized()) {
			output.writeByte(frame.length());
		} else if (frame.isVarShortSized()) {
			output.writeShort(frame.length());
		}

		// Write out the bytes of the frame
		output.writeBytes(frame.bytes());
	}

}
package us.cobalt.game.net.codec;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

import java.util.List;

import us.cobalt.game.net.frame.GameFrame;
import us.cobalt.game.net.msg.GameMessage;
import us.cobalt.game.net.msg.GameMessageCodec;

/**
 * Created by eve on 10/17/2014
 */
public final class GameMessageDecoder extends MessageToMessageDecoder<GameFrame> {

	private final GameMessageCodec codec;

	public GameMessageDecoder(GameMessageCodec codec) {
		this.codec = codec;
	}

	@Override
	protected void decode(ChannelHandlerContext ctx, GameFrame frame, List<Object> list) throws Exception {
		GameMessage message = codec.decode(frame);
		list.add(message);
	}

}
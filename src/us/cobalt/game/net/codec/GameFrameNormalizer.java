package us.cobalt.game.net.codec;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

import java.util.List;

import us.cobalt.game.net.frame.GameFrame;
import us.cobalt.game.net.obf.CodecNotFoundException;
import us.cobalt.game.net.obf.ObfCodecSet;

/**
 * Created by eve on 10/15/2014
 */
public final class GameFrameNormalizer extends MessageToMessageDecoder<GameFrame> {

	private final ObfCodecSet codecs;

	public GameFrameNormalizer(ObfCodecSet codecs) {
		this.codecs = codecs;
	}

	@Override
	protected void decode(ChannelHandlerContext channelHandlerContext, GameFrame frame, List<Object> objects) throws Exception {
		if (!frame.obfuscated()) {
			objects.add(frame);
		} else {
			// Check if there is a codec for the specified frame
			if (!codecs.exists(frame.id())) {
				throw new CodecNotFoundException("No codec exists for normalizing frame " + frame.name());
			}

			// Normalize the frame and send it along its way
			byte[] bytes = codecs.encode(frame.bytes(), frame.id());
			objects.add(new GameFrame(frame.meta(), bytes));
		}
	}

}
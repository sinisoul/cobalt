package us.cobalt.game.net.codec;

import static us.cobalt.game.net.frame.GameFrameMeta.VAR_BYTE_SIZED;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

import us.cobalt.game.net.Connection;
import us.cobalt.game.net.frame.FrameNotFoundException;
import us.cobalt.game.net.frame.GameFrame;
import us.cobalt.game.net.frame.GameFrameMetaSet;

/**
 * Created by eve on 10/15/2014
 */
public final class GameFrameDecoder extends ByteToMessageDecoder {

	private final Connection conn;
	private final GameFrameMetaSet frames;
	private State state = State.READ_ID;
	private int currentId, size, actualSize;

	private enum State {
		READ_ID, READ_SIZE, READ_PAYLOAD
	}

	public GameFrameDecoder(Connection conn, GameFrameMetaSet frames) {
		this.conn = conn;
		this.frames = frames;
	}

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf buf, List<Object> list) throws Exception {
		while (buf.isReadable()) {
			if (state == State.READ_ID) {

				// Read the current frame id from the buffer
				currentId = conn.isFramesCiphered() ? conn.decipherFrameId(buf.readByte()) : buf.readByte() & 0xff;

				// Check if the frame is specified in the meta set
				if (!frames.exists(currentId)) {
					throw new FrameNotFoundException("Unknown frame " + currentId);
				}

				size = actualSize = frames.sizeOf(currentId);

				// Update the state so that the size is read
				state = size >= 0 ? State.READ_PAYLOAD : State.READ_SIZE;
			}

			if (state == State.READ_SIZE) {

				// Check if the additional bytes to read the size are available
				// in the buffer
				int check = size == VAR_BYTE_SIZED ? 1 : 2;
				if (buf.isReadable(check)) {
					return;
				}

				// Read the size from the buffer
				actualSize = 0;
				for (int i = 0; i < check; i++) {
					actualSize |= (buf.readByte() & 0xff) << 8 * (check - 1 - i);
				}

				// Update the state so that the payload is read
				state = State.READ_PAYLOAD;
			}

			if (state == State.READ_PAYLOAD) {

				// Check if the payload for the frame can be read
				if (!buf.isReadable(actualSize)) {
					return;
				}

				// Read the bytes to the buffer for the frame
				byte[] bytes = new byte[actualSize];
				buf.readBytes(bytes);

				// Add the frame to the list of decoded objects
				list.add(new GameFrame(frames.forId(currentId), bytes));

				// Update the state so that the id is now read
				state = State.READ_ID;
			}

			// Continue looping as long as their is an id to be read
		}
	}

}
package us.cobalt.game.net.obf;

import java.util.HashMap;
import java.util.Map;

import us.cobalt.game.net.ByteBuffer;

/**
 * Created by eve on 10/15/2014
 */
public final class ObfCodecSet {

	private final Map<Integer, ObfCodec> codecs = new HashMap<>();

	public ObfCodecSet() {
	}

	public byte[] encode(byte[] bytes, int id) {
		return encode(bytes, 0, bytes.length, id);
	}

	public byte[] encode(byte[] bytes, int off, int len, int id) {
		ObfCodec codec = codecs.get(id);
		if (codec == null) {
			return new byte[0];
		}
		return codec.serialize(new ByteBuffer(bytes, off, len));
	}

	public int count() {
		return codecs.size();
	}

	public boolean exists(int id) {
		return codecs.containsKey(id);
	}

	public void bind(ObfCodec codec) {
		codecs.put(codec.id(), codec);
	}

}
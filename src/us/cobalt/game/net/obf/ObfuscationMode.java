package us.cobalt.game.net.obf;

/**
 * Created by eve on 10/15/2014
 */
public enum ObfuscationMode {

	OBFUSCATE,

	NORMALIZE

}
package us.cobalt.game.net.obf;

import org.luaj.vm2.LuaClosure;
import org.luaj.vm2.LuaValue;

/**
 * Created by eve on 10/15/2014
 */
public final class LuaObfCodec extends ObfCodec {

	private final LuaClosure closure;

	public LuaObfCodec(int id, ObfuscationMode mode, LuaClosure closure) {
		super(id, mode);
		this.closure = closure;
	}

	@Override
	protected void encode(ObfContext ctx) {
		closure.invoke(LuaValue.varargsOf(new LuaValue[] { new ObfContextTable(ctx) }));
	}

}
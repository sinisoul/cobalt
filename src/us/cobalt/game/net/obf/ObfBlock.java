package us.cobalt.game.net.obf;

import us.cobalt.game.net.ByteBuffer;

/**
 * Created by eve on 10/15/2014
 */
public final class ObfBlock implements Comparable<ObfBlock> {

	private final BufferVariable<?> var;
	private final int offset;

	public ObfBlock(BufferVariable<?> var, int offset) {
		this.var = var;
		this.offset = offset;
	}

	/**
	 * Serializes the block to the provided buffer.
	 *
	 * @param output The buffer to write the block to.
	 */
	public void serialize(ByteBuffer output) {
		var.serialize(output);
	}

	public int offset() {
		return offset;
	}

	@Override
	public int compareTo(ObfBlock block) {
		return Integer.compare(block.offset, offset);
	}

}
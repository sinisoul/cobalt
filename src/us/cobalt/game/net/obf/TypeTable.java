package us.cobalt.game.net.obf;

import static us.cobalt.game.net.obf.BufferVariable.BYTE;
import static us.cobalt.game.net.obf.BufferVariable.INT;
import static us.cobalt.game.net.obf.BufferVariable.JSTR;
import static us.cobalt.game.net.obf.BufferVariable.JSTR2;
import static us.cobalt.game.net.obf.BufferVariable.LONG;
import static us.cobalt.game.net.obf.BufferVariable.SHORT;

import org.luaj.vm2.LuaTable;

/**
 * Created by eve on 10/15/2014
 */
public final class TypeTable extends LuaTable {

	public TypeTable() {

		// Bind all of the basic variable types
		set("byte", BYTE);
		set("short", SHORT);
		set("int", INT);
		set("long", LONG);
		set("jstr", JSTR);

		// Bind all the obfuscated variable types
		set("jstr2", JSTR2);
	}

}
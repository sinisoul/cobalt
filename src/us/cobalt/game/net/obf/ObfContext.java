package us.cobalt.game.net.obf;

import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import us.cobalt.game.net.ByteBuffer;

/**
 * Created by eve on 10/15/2014
 */
public final class ObfContext {

	private final Queue<ObfBlock> blocks = new PriorityQueue<>();
	private final Set<Integer> offsets = new HashSet<>();
	private final ByteBuffer input;
	private final ObfuscationMode mode;

	public ObfContext(ByteBuffer input, ObfuscationMode mode) {
		this.input = input;
		this.mode = mode;
	}

	private void checkObfuscationMode(ObfuscationMode check) {
		if (mode != check) {
			throw new IllegalStateException();
		}
	}

	private void checkOffset(int offset) {
		if (offsets.contains(offset)) {
			throw new IllegalStateException("Cannot reuse the same offset in the codec");
		}
	}

	public ObfuscationMode getMode() {
		return mode;
	}

	public void read(int type, int offset) {
		checkObfuscationMode(ObfuscationMode.NORMALIZE);
		append(new ObfBlock(BufferVariable.decode(input, type, BufferVariable.normalize(type)), offset));
	}

	public void write(int type, int offset) {
		checkObfuscationMode(ObfuscationMode.OBFUSCATE);
		append(new ObfBlock(BufferVariable.decode(input, BufferVariable.normalize(type), type), offset));
	}

	public void append(ObfBlock block) {
		checkOffset(block.offset());
		offsets.add(block.offset());
		blocks.add(block);
	}

	public ByteBuffer aggregate() {
		ByteBuffer output = new ByteBuffer();
		blocks.forEach(block -> block.serialize(output));
		return output;
	}

}
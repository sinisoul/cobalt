package us.cobalt.game.net.obf;

/**
 * Created by eve on 10/15/2014
 */
public final class CodecNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1100506371976957538L;

	public CodecNotFoundException() {
		super();
	}

	public CodecNotFoundException(String message) {
		super(message);
	}

	public CodecNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public CodecNotFoundException(Throwable cause) {
		super(cause);
	}

}
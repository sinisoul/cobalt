package us.cobalt.game.net.obf;

import us.cobalt.game.net.ByteBuffer;

/**
 * Created by eve on 10/15/2014
 *
 * Encodes a byte buffer into a byte buffer that may be/may not be obfuscated.
 */
public abstract class ObfCodec {

	private final int id;
	private final ObfuscationMode mode;

	public ObfCodec(int id, ObfuscationMode mode) {
		this.id = id;
		this.mode = mode;
	}

	public byte[] serialize(ByteBuffer input) {

		// Encode the context to capture all of the blocks
		ObfContext context = new ObfContext(input, mode);
		encode(context);

		// Aggregate all of the blocks
		ByteBuffer output = context.aggregate();

		// Capture the bytes from the output byte buffer
		byte[] bytes = new byte[output.offset()];
		output.offset(0).get(bytes);
		return bytes;
	}

	protected abstract void encode(ObfContext context);

	public int id() {
		return id;
	}

}
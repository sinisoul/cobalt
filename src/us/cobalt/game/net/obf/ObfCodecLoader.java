package us.cobalt.game.net.obf;

import java.io.IOException;
import java.nio.file.Path;

import javax.script.ScriptException;

import us.cobalt.lua.ScriptManager;

/**
 * Created by eve on 10/15/2014
 */
public final class ObfCodecLoader {

	private final ScriptManager scriptManager = new ScriptManager();

	public ObfCodecLoader(ObfCodecSet codecs) {
		scriptManager.bindModule("codecs", new CodecTable(codecs));
	}

	public void loadFile(Path path) throws IOException, ScriptException {
		scriptManager.loadScript(path);
	}

	public ScriptManager getScriptManager() {
		return scriptManager;
	}

}
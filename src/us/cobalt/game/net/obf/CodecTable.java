package us.cobalt.game.net.obf;

import static us.cobalt.lua.LuaUtil.append;

import org.luaj.vm2.LuaClosure;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

/**
 * Created by eve on 10/15/2014
 */
public final class CodecTable extends LuaTable {

	private final ObfCodecSet set;

	public CodecTable(ObfCodecSet set) {
		append(this, "bind", (args) -> {

			// Get the codec mode for the specified type
			String obfuscationMode = args.checkjstring(1);
			ObfuscationMode mode = ObfuscationMode.valueOf(obfuscationMode.toUpperCase());

			// Get the key for the binding
			int id = args.checkint(2);

			// Get the closure for the
			LuaClosure closure = args.checkclosure(3);

			// Bind the handler to the codec set
			set.bind(new LuaObfCodec(id, mode, closure));

			// No return value as of now
			return LuaValue.NONE;
		});
		this.set = set;
	}

	public ObfCodecSet getSet() {
		return set;
	}

}
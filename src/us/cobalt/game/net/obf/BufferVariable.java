package us.cobalt.game.net.obf;

import java.io.Serializable;
import java.util.Set;

import us.cobalt.game.net.ByteBuffer;

import com.google.common.collect.Sets;

/**
 * Created by eve on 10/15/2014
 */
public final class BufferVariable<T> {

	// Set of all numeric classes, boxed and unboxed
	@SuppressWarnings("unchecked")
	private static final Set<Class<? extends Number>> numericClasses = Sets.newHashSet(Byte.class, Short.class, Integer.class, Long.class, byte.class, short.class, int.class, long.class);

	// Numeric types
	public static final int BYTE = 0;
	public static final int SHORT = 1;
	public static final int INT = 2;
	public static final int LONG = 3;

	// String types
	public static final int JSTR = 4;
	public static final int JSTR2 = 5;

	// Sets of all the types for checks
	private static final Set<Integer> numericTypes = Sets.newHashSet(BYTE, SHORT, INT, LONG);
	private static final Set<Integer> stringTypes = Sets.newHashSet(JSTR, JSTR2);
	private static final Set<Integer> types = Sets.union(numericTypes, stringTypes);

	private final T value;
	private final int type;

	public BufferVariable(T value, int type) {
		if (value == null) {
			throw new NullPointerException("Value cannot be null");
		}
		this.value = value;
		this.type = type;
		checkValidType();
	}

	private void checkValidType() {
		if (!types.contains(type)) {
			throw new IllegalArgumentException("Unknown type");
		}

		if (isNumericType() && !numericClasses.contains(value.getClass())) {
			throw new IllegalArgumentException("Mismatched types, expecting value to be a number");
		} else if (isStringType() && !value.getClass().equals(String.class)) {
			throw new IllegalArgumentException("Mismatched types, expecting value to be a string");
		}
	}

	public void serialize(ByteBuffer buf) {
		if (isNumericType()) {
			Number n = (Number) value;
			switch (type) {
			case BYTE:
				buf.putByte(n.intValue());
				break;
			case SHORT:
				buf.putShort(n.intValue());
				break;
			case INT:
				buf.putInt(n.intValue());
				break;
			case LONG:
				buf.putLong(n.longValue());
				break;
			}
		} else if (isStringType()) {
			String str = (String) value;
			switch (type) {
			case JSTR:
				buf.putJagstr(str);
				break;
			case JSTR2:
				buf.putJagstr2(str);
				break;
			default:
				throw new IllegalArgumentException("Unknown string type");
			}
		}
	}

	public T getValue() {
		return value;
	}

	public int getType() {
		return type;
	}

	public boolean isNumericType() {
		return isNumericType(type);
	}

	public boolean isStringType() {
		return isStringType(type);
	}

	public static boolean isNumericType(int type) {
		return numericTypes.contains(type);
	}

	public static boolean isStringType(int type) {
		return stringTypes.contains(type);
	}

	public static boolean isValidType(int type) {
		return types.contains(type);
	}

	public static int normalize(int type) {
		if (isNumericType(type)) {
			switch (type) {

			}
		} else if (isStringType(type)) {
			switch (type) {
			case JSTR2:
				return JSTR;
			}
		}

		return type;
	}

	public static BufferVariable<Serializable> decode(ByteBuffer input, int inputType, int type) {
		if (!isValidType(inputType)) {
			throw new IllegalArgumentException("Invalid input type");
		}

		if (isNumericType(inputType)) {
			Number num = null;
			switch (inputType) {
			case BYTE:
				num = input.getUByte();
				break;
			case SHORT:
				num = input.getUShort();
				break;
			case INT:
				num = input.getInt();
				break;
			case LONG:
				num = input.getLong();
				break;
			}
			return new BufferVariable<Serializable>(num, type);
		} else if (isStringType(inputType)) {
			String str = null;
			switch (inputType) {
			case JSTR:
				str = input.getJstr();
				break;
			case JSTR2:
				str = input.getJstr2();
				break;
			}
			return new BufferVariable<Serializable>(str, type);
		}
		throw new IllegalArgumentException("Unhandled type");
	}

}
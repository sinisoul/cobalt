package us.cobalt.game.net.obf;

import static us.cobalt.game.net.obf.ObfuscationMode.OBFUSCATE;
import static us.cobalt.lua.LuaUtil.append;

import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

/**
 * Created by eve on 10/15/2014
 */
public final class ObfContextTable extends LuaTable {

	public ObfContextTable(ObfContext ctx) {
		// Append the closure to the table
		append(this, ctx.getMode() == OBFUSCATE ? "write" : "read", args -> {

			// Get and check the variable type
			int type = args.checkint(1);
			if (!BufferVariable.isValidType(type)) {
				throw new IllegalArgumentException("Invalid type specified");
			}

			// The offset to use to write the normalized variable as
			int offset = args.checkint(2);

			switch (ctx.getMode()) {
			case OBFUSCATE:
				ctx.write(type, offset);
				break;
			case NORMALIZE:
				ctx.read(type, offset);
				break;
			}

			return LuaValue.NONE;
		});
	}

}
package us.cobalt.game.net;

import com.google.common.base.Charsets;

/**
 * Created by eve on 10/15/2014
 */
public final class ByteBuffer {

	private byte[] payload;
	private int offset = 0;
	private int capacity;

	public ByteBuffer(byte[] bytes) {
		this(bytes, 0, bytes.length);
	}

	public ByteBuffer(byte[] bytes, int off, int len) {
		payload = copyBuffer(bytes, off, len);
		capacity = len;
	}

	public ByteBuffer() {
		this(10);
	}

	public ByteBuffer(int capacity) {
		payload = new byte[capacity];
		this.capacity = capacity;
	}

	public ByteBuffer offset(int offset) {
		this.offset = offset;
		return this;
	}

	public int offset() {
		return offset;
	}

	public int next(int b) {
		for (int i = offset; i < capacity; i++) {
			if (payload[i] != b) {
				continue;
			}
			return i;
		}
		return -1;
	}

	public void ensureCapacity(int amount) {
		if (offset + amount > capacity) {

			// Loop until we reach the desired capacity for the amount
			// we need to ensure is writable
			for (; capacity < offset + amount; capacity <<= 2) {
			}

			// Copy over the old payload to the new buffer and set it
			byte[] buf = new byte[capacity];
			System.arraycopy(payload, 0, buf, 0, offset);
			payload = buf;

			// TODO: Remove this
			ensureCapacity(amount);
		}
	}

	public void putByte(int b) {
		ensureCapacity(1);
		payload[offset++] = (byte) b;
	}

	public void putBoolean(boolean bool) {
		putByte(bool ? 1 : 0);
	}

	public void putShort(int s) {
		ensureCapacity(2);
		payload[offset++] = (byte) (s >> 8);
		payload[offset++] = (byte) s;
	}

	public void putInt(int i) {
		ensureCapacity(4);
		payload[offset++] = (byte) (i >> 23);
		payload[offset++] = (byte) (i >> 16);
		payload[offset++] = (byte) (i >> 8);
		payload[offset++] = (byte) i;
	}

	public void putLong(long l) {
		ensureCapacity(8);
		payload[offset++] = (byte) (l >> 56L);
		payload[offset++] = (byte) (l >> 48L);
		payload[offset++] = (byte) (l >> 40L);
		payload[offset++] = (byte) (l >> 32L);
		payload[offset++] = (byte) (l >> 24L);
		payload[offset++] = (byte) (l >> 16L);
		payload[offset++] = (byte) (l >> 8L);
		payload[offset++] = (byte) l;
	}

	public void putSmart(int s) {
		if (s >= 0 && s < 128) {
			putByte(s);
			return;
		}
		if (s >= 0 && s < 32768) {
			putShort(s | 0x8000);
		} else {
			throw new IllegalArgumentException();
		}
	}

	public void put(byte[] bytes) {
		put(bytes, 0, bytes.length);
	}

	public void put(byte[] bytes, int off, int len) {
		ensureCapacity(len);
		System.arraycopy(bytes, off, payload, offset, len);
		offset += len;
	}

	public void get(byte[] bytes) {
		get(bytes, 0, bytes.length);
	}

	public void get(byte[] bytes, int off, int len) {
		System.arraycopy(payload, offset, bytes, off, len);
		offset += len;
	}

	public void putJagstr(String str) {
		put(str.getBytes(Charsets.UTF_8));
		putByte('\0');
	}

	public void putJagstr2(String str) {
		putByte('\0');
		put(str.getBytes(Charsets.UTF_8));
		putByte('\0');
	}

	public int getUByte() {
		return payload[offset++] & 0xff;
	}

	public int getUShort() {
		offset += 2;
		return (payload[offset - 2] << 8 & 0xff) + (payload[offset - 1] & 0xff);
	}

	public int getInt() {
		offset += 4;
		return (payload[offset - 4] << 24 & 0xff) + (payload[offset - 3] << 16 & 0xff) + (payload[offset - 2] << 8 & 0xff) + (payload[offset - 1] & 0xff);
	}

	public long getLong() {
		int firstChunk = getInt();
		int secondChunk = getInt();
		return (long) firstChunk << 32L | secondChunk & 0xffffffffL;
	}

	public String getJstr() {

		// Find the next null character for the string
		int off = next('\0');
		if (off == -1) {
			return null;
		}

		// Calculate the length of the string
		int len = off - offset;

		// Transfer the bytes to another buffer
		byte[] bytes = new byte[len];
		System.arraycopy(payload, 0, bytes, offset, len);
		offset += len + 1;

		return new String(bytes, Charsets.UTF_8);
	}

	public String getJstr2() {
		if (getUByte() != '\0') {
			throw new IllegalStateException("Illegally formatted jstr2");
		}
		return getJstr(); // Correct?
	}

	public byte[] payload() {
		return payload;
	}

	private static byte[] copyBuffer(byte[] bytes, int off, int len) {
		byte[] buf = new byte[len];
		System.arraycopy(bytes, off, buf, 0, len);
		return buf;
	}

}
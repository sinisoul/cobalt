package us.cobalt.game.net.frame;

public final class FrameNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -9017414285396601597L;

	public FrameNotFoundException() {
		super();
	}

	public FrameNotFoundException(String message) {
		super(message);
	}

	public FrameNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public FrameNotFoundException(Throwable cause) {
		super(cause);
	}

}
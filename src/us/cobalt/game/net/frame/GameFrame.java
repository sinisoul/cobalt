package us.cobalt.game.net.frame;

import us.cobalt.game.net.msg.GameMessage;

/**
 * Created by eve on 10/15/2014
 */
public final class GameFrame {

	private final GameFrameMeta meta;
	private final byte[] bytes;

	public GameFrame(GameFrameMeta meta, byte[] bytes) {
		this.bytes = bytes;
		this.meta = meta;
	}

	/**
	 * Gets the id of the frame.
	 *
	 * @return The id.
	 */
	public int id() {
		return meta.id();
	}

	/**
	 * Helper method. Gets if a frame has an id or if it is idless, idless packets include but are not limited to the world list frame. Not considered headerless seing as you should also write the size need be!
	 *
	 * @return If the frame is idless.
	 */
	public boolean hasId() {
		return meta.hasId();
	}

	public int obf() {
		return meta.obf();
	}

	public String name() {
		return meta.name();
	}

	public boolean obfuscated() {
		return meta.obfuscated();
	}

	public int length() {
		return bytes.length;
	}

	public byte[] bytes() {
		return bytes;
	}

	public boolean isVarByteSized() {
		return meta.varByteSized();
	}

	public boolean isVarShortSized() {
		return meta.varShortSized();
	}

	public GameFrameMeta meta() {
		return meta;
	}

	public Class<? extends GameMessage> messageClass() {
		return meta.messageClass();
	}

}
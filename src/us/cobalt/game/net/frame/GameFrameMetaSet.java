package us.cobalt.game.net.frame;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import us.cobalt.game.net.msg.GameMessage;
import us.cobalt.util.StringUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Created by eve on 10/15/2014
 */
public final class GameFrameMetaSet {

	private final Map<Class<? extends GameMessage>, GameFrameMeta> classToFrame = new HashMap<>();
	private final Map<Integer, GameFrameMeta> idToFrame = new HashMap<>();

	public GameFrameMetaSet() {
	}

	public void append(GameFrameMeta meta) {
		if (meta.hasId()) {
			idToFrame.put(meta.obfuscated() ? meta.obf() : meta.id(), meta);
		}
		classToFrame.put(meta.messageClass(), meta);
	}

	public int sizeOf(int id) {
		if (!exists(id)) {
			throw new FrameNotFoundException("No such frame " + id);
		}
		return forId(id).size();
	}

	public GameFrameMeta forId(int id) {
		return idToFrame.get(id);
	}

	public <T extends GameMessage> GameFrameMeta forClass(Class<T> messageClass) {
		return classToFrame.get(messageClass);
	}

	public boolean exists(int id) {
		return idToFrame.containsKey(id);
	}

	public <T extends GameMessage> boolean exists(Class<T> messageClass) {
		return classToFrame.containsKey(messageClass);
	}

	public List<GameFrameMeta> values() {
		return new ArrayList<>(classToFrame.values());
	}

	public void parseJSON(Path path) throws FileNotFoundException, ClassNotFoundException {
		parseJSON(new JsonParser().parse(new FileReader(path.toFile())).getAsJsonArray());
	}

	public void parseJSON(JsonArray array) throws ClassNotFoundException {
		for (JsonElement element : array) {
			JsonObject obj = element.getAsJsonObject();

			String name = obj.get("name").getAsString();
			int id = obj.has("id") ? obj.get("id").getAsInt() : -1;
			int obf = id != -1 && obj.has("obf") ? obj.get("obf").getAsInt() : -1;
			int size = obj.get("size").getAsInt();

			String classPrefix = StringUtils.camelize(obj.has("class") ? obj.get("class").getAsString() : name);
			append(new GameFrameMeta(classPrefix, name, id, size, obf));
		}
	}

}
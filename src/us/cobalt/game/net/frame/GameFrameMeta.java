package us.cobalt.game.net.frame;

import us.cobalt.game.net.msg.CodecNotFoundException;
import us.cobalt.game.net.msg.GameMessage;
import us.cobalt.game.net.msg.MessageDecoder;
import us.cobalt.game.net.msg.MessageEncoder;
import us.cobalt.game.net.msg.MessageNotFoundException;

/**
 * Created by eve on 10/15/2014
 */
public final class GameFrameMeta {

	public static final int VAR_BYTE_SIZED = -1;
	public static final int VAR_SHORT_SIZED = -2;

	private static final String PACKAGE = "us.cobalt.game.net.msg.";
	private static final String MESSAGE_SUFFIX = "%simpl.%sMessage";
	private static final String MESSAGE_DECODER_SUFFIX = "%sdecoder.%sMessageDecoder";
	private static final String MESSAGE_ENCODER_SUFFIX = "%sencoder.%sMessageEncoder";

	/**
	 * The message class the frame will either encode into or decode from.
	 */
	private Class<? extends GameMessage> messageClass;

	private Class<? extends MessageDecoder<?>> decoderClass;
	private Class<? extends MessageEncoder<?>> encoderClass;

	private final String prefix;
	private final String name;
	private final int size;
	private final int obf;
	private final int id;

	public GameFrameMeta(String prefix, String name, int id, int size, int obf) throws ClassNotFoundException {
		this.prefix = prefix;
		this.id = id;
		this.name = name;
		this.size = size;
		this.obf = obf;
	}

	public int id() {
		return id;
	}

	public int size() {
		return size;
	}

	public int obf() {
		return obf;
	}

	public String name() {
		return name;
	}

	public boolean hasId() {
		return id != -1;
	}

	/**
	 * Gets if the frame is obfuscated. If a frame is obfuscated the obfuscated id is written out to the client instead of its normal id.
	 *
	 * @return If the frame is obfuscated.
	 */
	public boolean obfuscated() {
		return obf != -1;
	}

	public boolean staticSized() {
		return size >= 0;
	}

	public boolean varByteSized() {
		return size == VAR_BYTE_SIZED;
	}

	public boolean varShortSized() {
		return size == VAR_SHORT_SIZED;
	}

	/**
	 * Gets the class of the message that the frame either is encoded or decoded from. If the class for the message could not be loaded then the method will throw a {@link java.lang.ClassNotFoundException}. Cold loads the class and stores it as a field to assure maximum efficiency.
	 *
	 * @return The class of the message that the frame encodes or decodes from.
	 *
	 * @throws MessageNotFoundException Thrown if the class of the message is not found.
	 *
	 */
	@SuppressWarnings("unchecked")
	public Class<? extends GameMessage> messageClass() throws MessageNotFoundException {
		if (messageClass == null) {
			try {
				String className = String.format(MESSAGE_SUFFIX, PACKAGE, prefix);
				messageClass = (Class<? extends GameMessage>) Class.forName(className);
			} catch (ClassNotFoundException reason) {
				throw new MessageNotFoundException(reason);
			}
		}
		return messageClass;
	}

	@SuppressWarnings("unchecked")
	public Class<? extends MessageDecoder<?>> decoderClass() throws CodecNotFoundException {
		if (decoderClass == null) {
			try {
				String className = String.format(MESSAGE_DECODER_SUFFIX, PACKAGE, prefix);
				decoderClass = (Class<? extends MessageDecoder<?>>) Class.forName(className);
			} catch (ClassNotFoundException reason) {
				throw new CodecNotFoundException(reason);
			}
		}
		return decoderClass;
	}

	@SuppressWarnings("unchecked")
	public Class<? extends MessageEncoder<?>> encoderClass() throws CodecNotFoundException {
		if (encoderClass == null) {
			try {
				String className = String.format(MESSAGE_ENCODER_SUFFIX, PACKAGE, prefix);
				encoderClass = (Class<? extends MessageEncoder<?>>) Class.forName(className);
			} catch (ClassNotFoundException reason) {
				throw new CodecNotFoundException(reason);
			}
		}
		return encoderClass;
	}

}
package us.cobalt.game;

import java.util.function.Consumer;

/**
 * Created by sini on 10/22/14.
 */
public interface ClientHandler<T> extends Consumer<T> {

}
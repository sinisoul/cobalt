package us.cobalt.game;

/**
 * Created by sini on 10/23/14.
 */
public final class ResponseStatus {

	public static final int STATUS_OKAY = 0;
	public static final int STATUS_FULL = 7;
	public static final int STATUS_CONNECTION_LIMIT_EXCEEDED = 9;

	private ResponseStatus() {
	}

}
package us.cobalt.game;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ExecutionException;

import javax.script.ScriptException;

import org.luaj.vm2.LuaTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.cobalt.InitializationException;
import us.cobalt.game.login.LoginService;
import us.cobalt.game.net.Connection;
import us.cobalt.game.net.InboundMessageHandler;
import us.cobalt.game.net.codec.GameFrameDecoder;
import us.cobalt.game.net.codec.GameFrameEncoder;
import us.cobalt.game.net.codec.GameFrameNormalizer;
import us.cobalt.game.net.codec.GameFrameObfuscator;
import us.cobalt.game.net.codec.GameMessageDecoder;
import us.cobalt.game.net.codec.GameMessageEncoder;
import us.cobalt.game.net.frame.GameFrameMeta;
import us.cobalt.game.net.frame.GameFrameMetaSet;
import us.cobalt.game.net.msg.ConnectionMessageDispatcher;
import us.cobalt.game.net.msg.MessageHandlerSet;
import us.cobalt.game.net.msg.GameMessageCodec;
import us.cobalt.game.net.msg.handler.CheckShardActiveHandler;
import us.cobalt.game.net.msg.handler.GameServiceHandshakeHandler;
import us.cobalt.game.net.msg.impl.RefreshWorldlistMessage;
import us.cobalt.game.net.obf.ObfCodecLoader;
import us.cobalt.game.net.obf.ObfCodecSet;
import us.cobalt.game.worldlist.WorldlistClient;
import us.cobalt.game.worldlist.WorldlistService;

import com.google.common.collect.Sets;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Created by sini on 10/23/14.
 */
public final class GameNode extends GameService<NodeClient> {

	private static final Logger logger = LoggerFactory.getLogger(GameNode.class);
	private final ObfCodecSet frameCodecs = new ObfCodecSet();
	private final GameFrameMetaSet clientFrames = new GameFrameMetaSet();
	private final GameFrameMetaSet serverFrames = new GameFrameMetaSet();
	private final GameMessageCodec messageCodec = new GameMessageCodec(serverFrames);
	private final ServerBootstrap bootstrap = new ServerBootstrap();

	@Override
	public void load() {
		// Load all of the frame meta information
		loadFrames(Paths.get(configDir(), "frames.json"));

		// Load all of the frame obfuscation codecs
		loadFrameCodecs(Paths.get(configDir(), "codecs/frames.lua"));

		// Limit the amount of players, and connections.
		limitClients(getInt("player_limit", 50));
		limitConnections(getInt("conn_limit", 1));

		// Load all of the message decoders and encoders
		loadMessageDecoders();
		loadMessageEncoders();

		// Bind the game service to either the port specified in the config or 40001
		onStart(() -> bind(new InetSocketAddress(40000 + getInt("node", 1))));
	}

	public void loadFrameCodecs(Path path) {
		try {
			// Just create a quick lookup table for all of the frames
			LuaTable table = new LuaTable();
			Sets.union(Sets.newHashSet(clientFrames.values()), Sets.newHashSet(serverFrames.values())).forEach(meta -> table.set(meta.name(), meta.id()));

			// Load the frame codecs at the specified location
			ObfCodecLoader loader = new ObfCodecLoader(frameCodecs);
			loader.getScriptManager().bindModule("frames", table);
			loader.loadFile(path);
		} catch (IOException | ScriptException reason) {
			logger.error("Failed to load the frame obfuscation codecs", reason);
			throw new InitializationException(reason);
		}
	}

	public void loadFrames(Path path) {
		try {

			// Parse the JSON file at the specified path as an object where you read both the
			// server and client frames from the single file
			JsonObject obj = new JsonParser().parse(new FileReader(path.toFile())).getAsJsonObject();

			// Attempt to parse both the server and client frames from the array
			if (obj.has("client")) {
				clientFrames.parseJSON(obj.getAsJsonArray("client"));
			}
			if (obj.has("server")) {
				serverFrames.parseJSON(obj.getAsJsonArray("server"));
			}
		} catch (FileNotFoundException | ClassNotFoundException reason) {
			logger.error("Failed to load the frame config", reason);
			throw new InitializationException(reason);
		}
	}

	public void loadMessageDecoders() {
		try {
			// Load all of the decoders from the registered frames
			for (GameFrameMeta meta : clientFrames.values()) {
				messageCodec.append(meta.decoderClass().newInstance());
			}
		} catch (InstantiationException | IllegalAccessException reason) {
			logger.error("Could not instantiate message decoder", reason);
			throw new InitializationException(reason);
		}
	}

	public void loadMessageEncoders() {
		try {
			// Load all of the encoders from the registered frames
			for (GameFrameMeta meta : serverFrames.values()) {
				messageCodec.append(meta.encoderClass().newInstance());
			}
		} catch (InstantiationException | IllegalAccessException reason) {
			logger.error("Could not instantiate message encoder", reason);
			throw new InitializationException(reason);
		}
	}

	public void bind(SocketAddress address) {
		try {
			logger.info("Binding the game service to {}", address);

			MessageHandlerSet handlers = new MessageHandlerSet();

			// If the world list service is present then append the handshake handler
			// so that we can register incoming connections to the service.
			if (serviceExists(WorldlistService.class)) {
				handlers.append(new GameServiceHandshakeHandler<>(RefreshWorldlistMessage.class, getService(WorldlistService.class), (conn, msg) -> new WorldlistClient(conn)));
			}

            // If the login service is present then append the check shard active
            // handler to assure that a specific shard is available for login.
            // Seeing as this is in the distant future, I wouldn't worry about
            // it too much you nerd.
            if(serviceExists(LoginService.class)) {
                handlers.append(new CheckShardActiveHandler());
            }

			// Setup the bootstrap
			bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
				@Override
				protected void initChannel(SocketChannel ch) throws Exception {
					Connection conn = new Connection(ch);

					// Set the connection message dispatcher
					conn.setDispatcher(new ConnectionMessageDispatcher(conn, handlers));

					// Setup the channel pipeline
					ChannelPipeline pipeline = ch.pipeline();

					// Decoder pipeline
					// ----------------
					// Bytes -> Obfuscated Frame -> Normalized Frame -> Message
					//
					// We read bytes sent from the client and decode frames, we then normalize
					// the frame which means taking the frame stream and removing all obfuscation,
					// then we take the frame and decode a message from that and the
					// registered class to the frame that the message will be decoded into.
					//
					// All messages are then dumped into the inbound message handler.
					pipeline.addLast(new GameFrameDecoder(conn, clientFrames)).
						addLast(new GameFrameNormalizer(frameCodecs)).
						addLast(new GameMessageDecoder(messageCodec)).
						addLast(new InboundMessageHandler(conn));

					// Encoder pipeline
					// ----------------
					// Message -> Normalized Frame -> Obfuscated Frame -> Bytes
					//
					// We take the game message and then encode it into a frame, we then obfuscate the frame
					// which is considered normalized after its been encoded if we need to, then we take
					// the obfuscated frame and encode it into bytes to write out to the client.
					pipeline.addLast(new GameFrameEncoder(conn)).
						addLast(new GameFrameObfuscator(frameCodecs)).
						addLast(new GameMessageEncoder(messageCodec));
				}
			});

			// Get the show on the road!
			bootstrap.channel(NioServerSocketChannel.class).group(new NioEventLoopGroup()).localAddress(address).bind().get();
		} catch (ExecutionException | InterruptedException ex) {
			logger.error("Could not bind game service", ex);
			throw new InitializationException(ex);
		}
	}

}
package us.cobalt.game;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by sini on 10/21/14.
 */
public final class ClientList<T extends GameClient> implements Iterable<T> {

	private GameClient[] array;
	private int capacity, size, modCount;

	public ClientList() {
		this(10);
	}

	public ClientList(int capacity) {
		array = new GameClient[capacity];
		this.capacity = capacity;
	}

	private void checkId(int id) {
		if (id < 0 || id >= capacity) {
			throw new ArrayIndexOutOfBoundsException("Id out of bounds");
		}
	}

	private int nextSlot() {
		for (int i = 0; i < capacity; i++) {
			if (array[i] != null) {
				continue;
			}
			return i;
		}
		return -1;
	}

	private void checkCapacity(int amount) {
		if (size + amount > capacity) {
			GameClient[] arr = new GameClient[capacity <<= 2];
			System.arraycopy(array, 0, arr, 0, array.length);
			array = arr;
		}
	}

	public boolean add(T client) {
		synchronized (this) {

			// Check that the client doesnt belong to another list
			if (client.id() != -1) {
				throw new IllegalArgumentException("Client already belongs to another list");
			}

			// Check that we have enough room
			checkCapacity(1);

			// Get the next slot to append the client to in the array
			int id = nextSlot();
			assert id != -1;

			// Add the client to the array and set the id of the client
			array[id] = client;
			client.setId(id);
			size++;

			// Increment the modification count
			modCount++;

			return true;
		}
	}

	@SuppressWarnings("unchecked")
	public T get(int id) {
		checkId(id);
		return (T) array[id];
	}

	public void remove(T client) {
		synchronized (this) {
			// I mean if the client belongs to the list it cant be -1
			assert client.id() != -1;

			if (array[client.id()] != client) {
				throw new IllegalArgumentException("Provided client to remove from list doesn't belong to this list");
			}

			// Remove the client from the array at its specified id and reset its id
			array[client.id()] = null;
			client.setId(-1);
			size--;

			// Increment the modification count
			modCount++;
		}
	}

	public int size() {
		synchronized (this) {
			return size;
		}
	}

	private class Itr implements Iterator<T> {
		int cursor;
		int lastRet = -1;
		int expectedModCount = modCount;

		@Override
		public boolean hasNext() {
			return cursor != size;
		}

		@Override
		@SuppressWarnings("unchecked")
		public T next() {
			checkForComodification();
			int i = cursor;
			if (i >= size) {
				throw new NoSuchElementException();
			}
			GameClient[] elementData = ClientList.this.array;
			if (i >= elementData.length) {
				throw new ConcurrentModificationException();
			}
			cursor = i + 1;
			return (T) elementData[lastRet = i];
		}

		@Override
		@SuppressWarnings("unchecked")
		public void remove() {
			if (lastRet < 0) {
				throw new IllegalStateException();
			}
			checkForComodification();
			try {
				ClientList.this.remove((T) array[lastRet]);
				cursor = lastRet;
				lastRet = -1;
				expectedModCount = modCount;
			} catch (IndexOutOfBoundsException ex) {
				throw new ConcurrentModificationException();
			}
		}

		final void checkForComodification() {
			if (modCount != expectedModCount) {
				throw new ConcurrentModificationException();
			}
		}
	}

	@Override
	public Iterator<T> iterator() {
		return new Itr();
	}

}
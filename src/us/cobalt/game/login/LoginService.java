package us.cobalt.game.login;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.cobalt.InitializationException;
import us.cobalt.game.GameService;
import us.cobalt.game.RSAKeypair;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by sini on 10/24/14.
 */
public final class LoginService extends GameService<LoginClient> {

    private static final Logger logger = LoggerFactory.getLogger(LoginService.class);
    private RSAKeypair keypair;

    @Override
    public void load() {
        // Load the RSA keypair from disk, used for encrypting sensitive data
        // during login such as the username, password, and ISAAC seeds.
        loadRSAKeypair(Paths.get(dataDir(), "keypair.json"));
    }

    public void loadRSAKeypair(Path path) {
        try {
            logger.info("Loading RSA keypair from disk");
            keypair = RSAKeypair.loadFromJSON(path);
        } catch(IOException reason) {
            logger.error("Failed to load the RSA keypair from disk", reason);
            throw new InitializationException(reason);
        }
    }
}

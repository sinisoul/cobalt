package us.cobalt.game.login;

import us.cobalt.game.GameClient;
import us.cobalt.game.net.Connection;

/**
 * Created by sini on 10/24/14.
 */
public final class LoginClient extends GameClient {
    public LoginClient(Connection conn) {
        super(conn);
    }
}
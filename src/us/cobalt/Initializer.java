package us.cobalt;

/**
 * Created by sini on 10/22/14.
 */
public interface Initializer {
	void execute();
}
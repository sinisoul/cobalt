package us.cobalt;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Created by sini on 8/17/14.
 */
public final class Server {

	private static final Logger logger = LoggerFactory.getLogger(Server.class);
	private final ListMultimap<Class<? extends Service>, Service> services = ArrayListMultimap.create();
	private final Environment env;

	public Server(Environment env) {
		this.env = env;
	}

	@SuppressWarnings("unchecked")
	public void loadServices() {
		JsonArray array;
		try {
			Path path = Paths.get(env.configDir(), "services.json");
			array = new JsonParser().parse(Files.newBufferedReader(path)).getAsJsonArray();
		} catch (IOException reason) {
			logger.error("Failed to load the services config file", reason);
			throw new InitializationException(reason);
		}

		try {
			for (JsonElement element : array) {
				JsonObject obj = element.getAsJsonObject();

				String clsName = obj.get("class").getAsString();
				String name = obj.get("name").getAsString();

				Class<? extends Service> cls = (Class<? extends Service>) Class.forName(clsName);

				Service service = cls.newInstance();
				service.name(name);

				appendService(service);
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException reason) {
			logger.error("Failed to load the server services", reason);
			throw new InitializationException(reason);
		}
	}

	public void appendService(Service service) {
		service.context(new ServerContext(this));
		services.put(service.getClass(), service);
	}

	public <T extends Service> boolean serviceExists(Class<T> cls) {
		return services.containsKey(cls);
	}

	public <T extends Service> T getService(Class<T> cls) {
		return getService(cls, 0);
	}

	@SuppressWarnings("unchecked")
	public <T extends Service> T getService(Class<T> cls, int index) {
		List<Service> list = services.get(cls);
		return (T) list.get(index);
	}

	public Collection<Service> services() {
		return services.values();
	}

	public Environment env() {
		return env;
	}

	public void start() {
		services().forEach(service -> service.init());
		services().forEach(service -> service.start());
	}

}
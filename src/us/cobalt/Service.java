package us.cobalt;

import static us.cobalt.Environment.DEVELOPMENT_ENV;
import static us.cobalt.Environment.PRODUCTION_ENV;
import static us.cobalt.Environment.TESTING_ENV;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

/**
 * Created by sini on 8/17/14.
 */
public abstract class Service {

	/**
	 * All of the service states.
	 */
	protected enum State {
		START;
	}

	/**
	 * The mapping of initializers and their states.
	 */
	private final Multimap<State, Initializer> initializers = HashMultimap.create();

	/**
	 * The server context to which the service is registered to.
	 */
	private ServerContext context;

	/**
	 * The id of the service.
	 */
	private String name = "";

	/**
	 * Constructs a new {@link Service};
	 */
	public Service() {
	}

	/**
	 * Sets the server context.
	 *
	 * @param context The server context.
	 */
	public void context(ServerContext context) {
		this.context = context;
	}

	/**
	 * Gets the server context.
	 *
	 * @return The server context.
	 */
	public ServerContext context() {
		return context;
	}

	/**
	 * Sets the id of the service.
	 *
	 * @param name The id.
	 */
	public void name(String name) {
		this.name = name;
	}

	/**
	 * Gets the id of the service.
	 *
	 * @return The id.
	 */
	public String name() {
		return name;
	}

	/**
	 * Gets if a service exists.
	 *
	 * @param cls The class of the service.
	 * @param <T> The generic service type.
	 * @return If the service exists.
	 */
	public <T extends Service> boolean serviceExists(Class<T> cls) {
		return context.serviceExists(cls);
	}

	/**
	 * Gets a service.
	 *
	 * @param cls The class of the service.
	 * @param <T> The generic service type.
	 * @return The service for the specified class.
	 */
	public <T extends Service> T getService(Class<T> cls) {
		return context.getService(cls);
	}

	/**
	 * Gets an int from the environment.
	 *
	 * @param key The lookup key.
	 * @param def The default int.
	 * @return If the environment contains a value at the specified key, the value otherwise the default provided int.
	 */
	public int getInt(String key, int def) {
		String str = String.format("%s.%s", name(), key);
		return env().get(str, def);
	}

	/**
	 * Gets a string from the environment.
	 *
	 * @param key The lookup key.
	 * @param def The default string.
	 * @return If the environment contains a value at the specified key, the value otherwise the default provided string.
	 */
	public String getString(String key, String def) {
		String str = String.format("%s.%s", name(), key);
		return env().get(str, def);
	}

	/**
	 * Gets the environment.
	 *
	 * @return The environment.
	 */
	public Environment env() {
		return context.env();
	}

	/**
	 * Gets the configuration directory based upon which environment the service is in.
	 *
	 * @return The configuration directory.
	 */
	public String configDir() {
		return env().configDir();
	}

    public String dataDir() { return "data/"; }

	/**
	 * Gets if the service is in one of the provided environments.
	 *
	 * @param envs The list of environments to test if the service is in.
	 * @return If the service is in one of the provided environments.
	 */
	public boolean inEnvironment(int... envs) {
		for (int env : envs) {
			switch (env) {
			case TESTING_ENV:
				if (inTesting()) {
					return true;
				}
				break;
			case DEVELOPMENT_ENV:
				if (inDevelopment()) {
					return true;
				}
				break;
			case PRODUCTION_ENV:
				if (inProduction()) {
					return true;
				}
				break;
			}
		}
		return false;
	}

	/**
	 * Gets if the service is in the testing environment.
	 *
	 * @return If the service is in the testing environment.
	 */
	public boolean inTesting() {
		return env().isTesting();
	}

	/**
	 * Gets if the service is in the development environment.
	 *
	 * @return If the service is in the development environment.
	 */
	public boolean inDevelopment() {
		return env().isDevelopment();
	}

	/**
	 * Gets if the service is in the production environment.
	 *
	 * @return If the service is in the production environment.
	 */
	public boolean inProduction() {
		return env().isProduction();
	}

	/**
	 * Helper method. Gets the environment id.
	 *
	 * @return The environment id.
	 */
	public String envName() {
		return env().name();
	}

	/**
	 * Registers an initializer for when the server starts.
	 *
	 * @param initializer The initializer.
	 */
	public void onStart(Initializer initializer) {
		on(State.START, initializer);
	}

	/**
	 * Appends an initializer to be ran when the service's state is changed.
	 *
	 * @param state The state to bind the initializer to.
	 * @param initializer The initializer.
	 */
	public void on(State state, Initializer initializer) {
		initializers.put(state, initializer);
	}

	/**
	 * Executes the initializers for a specific state.
	 *
	 * @param state The state.
	 */
	private void executeInitializers(State state) {
		initializers.get(state).forEach(initializer -> initializer.execute());
	}

	/**
	 * Initializes the service.
	 */
	public abstract void init();

	/**
	 * Starts the service.
	 */
	public void start() {
		executeInitializers(State.START);
	}

}
package us.cobalt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Created by sini on 8/17/14.
 */
public final class Environment {

	private static final String[] ENVS = { "testing", "development", "production" };

	public static final int TESTING_ENV = 0;
	public static final int DEVELOPMENT_ENV = 1;
	public static final int PRODUCTION_ENV = 2;

	private final Map<String, String> values = new HashMap<>();
	private final int type;

	public Environment(int type) {
		this.type = type;
	}

	public void store(String key, String value) {
		values.put(key, value);
	}

	public void store(String key, int value) {
		store(key, Integer.toString(value));
	}

	public String getString(String key) {
		return values.get(key);
	}

	public Integer getInt(String key) {
		return Integer.parseInt(getString(key));
	}

	public String get(String key, String def) {
		if (!exists(key)) {
			return def;
		}
		return getString(key);
	}

	public Integer get(String key, int def) {
		if (!exists(key)) {
			return def;
		}
		return Integer.parseInt(getString(key));
	}

	public List<String> startingWith(String str) {
		List<String> list = new ArrayList<>();
		for (String key : values.keySet()) {
			if (key.startsWith(str)) {
				list.add(key);
			}
		}
		return list;
	}

	public boolean exists(String key) {
		return values.containsKey(key);
	}

	public boolean isTesting() {
		return type == TESTING_ENV;
	}

	public boolean isDevelopment() {
		return type == DEVELOPMENT_ENV;
	}

	public boolean isProduction() {
		return type == PRODUCTION_ENV;
	}

	public String name() {
		return ENVS[type];
	}

	public String configDir() {
		return "data/config/" + name();
	}

	public void parseArguments(String[] args) {
		for (String arg : args) {
			if (!arg.startsWith("--D")) {
				continue;
			}

			String str = arg.substring(3);
			if (str.indexOf('=') == -1) {
				throw new IllegalArgumentException("Illegally formatted environment variable");
			}
			store(str.substring(0, str.indexOf('=')), str.substring(str.indexOf('=') + 1));
		}
	}

	public void parseJSON(JsonObject obj) {
		if (!obj.has(name())) {
			return;
		}
		parseJSON("", obj.get(name()).getAsJsonObject());
	}

	private void parseJSON(String prefix, JsonObject obj) {
		for (Entry<String, JsonElement> entry : obj.entrySet()) {
			JsonElement element = entry.getValue();
			if (element.isJsonArray()) {
				throw new UnsupportedOperationException("Cannot parse JSON arrays");
			}
			if (element.isJsonObject()) {
				parseJSON(appendPrefix(prefix, entry.getKey()), element.getAsJsonObject());
			}
			if (element.isJsonPrimitive()) {
				store(appendPrefix(prefix, entry.getKey()), element.getAsString());
			}
		}
	}

	private String appendPrefix(String prefix, String str) {
		return prefix + (!prefix.equals("") ? "." : "") + str;
	}

	public static Environment forType(String name) {
		for (int type = 0; type < ENVS.length; type++) {
			if (name.toLowerCase().equals(ENVS[type])) {
				return new Environment(type);
			}
		}
		throw new IllegalArgumentException("Unknown environment type " + name);
	}

}
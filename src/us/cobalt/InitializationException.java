package us.cobalt;

/**
 * Created by eve on 10/14/2014
 */
public final class InitializationException extends RuntimeException {

	private static final long serialVersionUID = -5285391029262581821L;

	public InitializationException() {
		super();
	}

	public InitializationException(String message) {
		super(message);
	}

	public InitializationException(String message, Throwable cause) {
		super(message, cause);
	}

	public InitializationException(Throwable cause) {
		super(cause);
	}

}